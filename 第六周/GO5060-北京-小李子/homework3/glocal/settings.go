package glocal

import (
	"github.com/spf13/viper"
	"log"
	"os"
	"time"
)

type ServerSetting struct {
	HttpIp string
	HttpPort int
	HttpTrace time.Duration
}

func NewSetting(config string) (*viper.Viper, error) {
	vp := viper.New()
	vp.SetConfigName("config")
	vp.AddConfigPath(config)
	vp.SetConfigType("yaml")
	err := vp.ReadInConfig()
	if err != nil {
		return nil, err
	}
	return vp, nil
}

func New() *ServerSetting {
	s, err := NewSetting("configs")
	if err != nil {
		log.Printf("init config err: %v", err)
		os.Exit(1)
	}
	var serverSetting *ServerSetting
	err = s.UnmarshalKey("http", &serverSetting)
	if err != nil {
		log.Printf("init config err: %v", err)
		os.Exit(1)
	}
	return serverSetting
}
