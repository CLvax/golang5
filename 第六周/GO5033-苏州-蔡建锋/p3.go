package GO5033_苏州_蔡建锋

/**
 * @Classname p2
 * @Description TODO
 * @author cjf
 * @Date 2021/7/14 18:19
 * @Version V1.0
 */

//- 实现一个简单的http探测的web
//- gin写一个web /probe/http?host=baidu.com&is_https=1
//- host代表探测的地址或ip
//- is_https=1代表探测 https://baidu.com否则是  http://baidu.com
//- 返回探测的结果
//- 域名的ip
//- status_code
//- http各阶段的耗时
//- 需要一个yaml解析的配置
//- http的listen的地址
//- 探测超时时间
//- 总的来说就是实现上述工程，go mod管理
