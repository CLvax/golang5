package main

import (
	testtool "github.com/hujidong/testtool"
	testtool2 "github.com/hujidong/testtool/v2"
)

func main() {
	testtool.GetIp()
	testtool.GetHostName()
	testtool2.GetHostName("lalala")
}

/*
[Running] go run "e:\golang3\day6\testToolmain\main.go"
2021/07/16 17:24:57 192.168.200.2
2021/07/16 17:24:57 star15g
2021/07/16 17:24:57 star15glalala
*/
