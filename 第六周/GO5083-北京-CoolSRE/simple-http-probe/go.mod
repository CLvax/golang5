module simple-http-probe

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2 // indirect
	github.com/go-yaml/yaml v2.1.0+incompatible
)
