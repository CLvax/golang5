package main

import (
	"flag"
	"log"
	"simple-http-probe/config"
	"simple-http-probe/http"
)

/*
- 实现一个简单的http探测的web
- gin写一个web /probe/http?host=baidu.com&is_https=1
- host代表探测的地址或ip
- is_https=1代表探测 https://baidu.com否则是  http://baidu.com
- 返回探测的结果
    - 域名的ip
    - status_code
    - http各阶段的耗时
- 需要一个yaml解析的配置
    - http的listen的地址
    - 探测超时时间
- 总的来说就是实现上述工程，go mod管理
*/
var (
	configFile string
)

func main() {
	// 处理配置文件
	flag.StringVar(&configFile, "c", "simple_http_probe.yml", "config file path")
	conf, err := config.LoadFile(configFile)
	if err != nil {
		log.Printf("[config.Load.error][err:%v]", err)
		return
	}
	log.Printf("[配置是:%v]", conf)
	// 启动gin
	go http.StartGin(conf)
	select {}
}
