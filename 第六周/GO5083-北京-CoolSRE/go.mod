module gittool

go 1.16

require (
	github.com/coolber/tools v1.0.1
	github.com/go-yaml/yaml v2.1.0+incompatible // indirect
)
