package global

import (
	"fmt"
	"simplehttpprobe/config"
)

var Gconfig *config.Config

func init() {
	var err error
	Gconfig, err = config.ParseConfig()
	if err != nil {
		fmt.Println(err)
	}
}
