package http

import (
	"net/http"
	"simplehttpprobe/config"

	"github.com/gin-gonic/gin"
)

func StartGin(config *config.Config) {
	// 初始化gin实例
	r := gin.Default()
	// 绑定路由
	Routes(r)
	r.Run(config.HttpServerListen)
}

func Routes(r *gin.Engine) {
	api := r.Group("api")
	{
		api.GET("/probe/http", Probe)
		api.GET("v1", func(c *gin.Context) {
			c.String(http.StatusOK, "你好,我是http probe")
		})
	}
}
