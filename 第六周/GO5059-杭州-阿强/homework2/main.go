package main

import (
	"simplehttpprobe/global"

	"simplehttpprobe/http"
)

func main() {

	go http.StartGin(global.Gconfig)
	select {}
}
