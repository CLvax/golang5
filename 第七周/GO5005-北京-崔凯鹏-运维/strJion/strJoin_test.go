package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"strings"
	"testing"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

/*
- 使用+拼接
- 使用fmt.sprinf拼接
- 使用strings.builder拼接
- 使用bytes.buffer拼接
- 使用[]byte拼接
- 生成基本字符串的函数如下
*/

// randomStringSprinf: 使用fmt.sprinf拼接
func randomStringSprinf(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return fmt.Sprintf("%s", b)
}

// randomStringByte: 使用[]byte拼接字符串
func randomStringByte(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

// randomStringSbuilder: 使用strings.builder拼接
func randomStringSbuilder(n int) string {
	s := strings.Builder{}
	for i := 0; i < n; i++ {
		s.WriteByte(-letterBytes[rand.Intn(len(letterBytes))])
	}
	return s.String()
}

// randomStringBbuilder:  使用bytes.builder拼接
func randomStringBbuilder(n int) string {
	b := bytes.Buffer{}
	for i := 0; i < n; i++ {
		b.WriteByte(letterBytes[rand.Intn(len(letterBytes))])
	}
	return b.String()
}

// randomStringJoin： 使用+拼接字符串
func randomStringJoin(n int) string {
	var j string
	for i := 0; i < len(letterBytes); i++ {
		j += string(letterBytes[rand.Intn(len(letterBytes))])
	}
	return j
}

func benchmarkRandomStringSprinf(i int, b *testing.B) {
	for n:=0;n<b.N;n++{
		randomStringSprinf(i)
	}
	
}

func benchmarkRandomRandomStringByte(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		randomStringByte(i)

	}
}

// // randomStringSbuilder
func benchmarkRandomRandomStringSbuilder(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		randomStringSbuilder(i)
	}

}

// // randomStringBbuilder
func benchmarkRandomRandomStringBbuilder(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		randomStringBbuilder(n)
	}

}

// randomStringJoin
func benchmarkRandomRandomStringJoin(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		randomStringJoin(n)
	}

}

func BenchmarkRandomRandomStringJoin10(b *testing.B)    { benchmarkRandomRandomStringJoin(10, b) }
func BenchmarkRandomRandomStringJoin100(b *testing.B)   { benchmarkRandomRandomStringJoin(100, b) }
func BenchmarkRandomRandomStringJoin1000(b *testing.B)  { benchmarkRandomRandomStringJoin(1000, b) }
func BenchmarkRandomRandomStringJoin10000(b *testing.B) { benchmarkRandomRandomStringJoin(10000, b) }

func BenchmarkRandomRandomStringBbuilder100(b *testing.B) {
	benchmarkRandomRandomStringBbuilder(100, b)
}
func BenchmarkRandomRandomStringBbuilder1000(b *testing.B) {
	benchmarkRandomRandomStringBbuilder(1000, b)
}
func BenchmarkRandomRandomStringBbuilder10000(b *testing.B) {
	benchmarkRandomRandomStringBbuilder(10000, b)
}
func BenchmarkRandomRandomStringBbuilder100000(b *testing.B) {
	benchmarkRandomRandomStringBbuilder(100000, b)
}

func BenchmarkRandomRandomStringSbuilder100(b *testing.B) {
	benchmarkRandomRandomStringSbuilder(100, b)
}
func BenchmarkRandomRandomStringSbuilder1000(b *testing.B) {
	benchmarkRandomRandomStringSbuilder(1000, b)
}
func BenchmarkRandomRandomStringSbuilder10000(b *testing.B) {
	benchmarkRandomRandomStringSbuilder(10000, b)
}
func BenchmarkRandomRandomStringSbuilder100000(b *testing.B) {
	benchmarkRandomRandomStringSbuilder(100000, b)
}
func BenchmarkRandomRandomStringSbuilder1000000(b *testing.B) {
	benchmarkRandomRandomStringSbuilder(100000, b)
}

func BenchmarkRandomRandomStringByte100(b *testing.B)    { benchmarkRandomRandomStringByte(100, b) }
func BenchmarkRandomRandomStringByte1000(b *testing.B)   { benchmarkRandomRandomStringByte(1000, b) }
func BenchmarkRandomRandomStringByte10000(b *testing.B)  { benchmarkRandomRandomStringByte(10000, b) }
func BenchmarkRandomRandomStringByte100000(b *testing.B) { benchmarkRandomRandomStringByte(100000, b) }

func BenchmarkRandomStringSprinf100(b *testing.B) { benchmarkRandomStringSprinf(100,b) }
func BenchmarkRandomStringSprinf1000(b *testing.B) { benchmarkRandomStringSprinf(1000,b) }
func BenchmarkRandomStringSprinf10000(b *testing.B) { benchmarkRandomStringSprinf(10000,b) }
func BenchmarkRandomStringSprinf100000(b *testing.B) { benchmarkRandomStringSprinf(100000,b) }
