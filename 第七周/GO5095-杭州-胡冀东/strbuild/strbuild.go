package strbuild

import (
	"bytes"
	"fmt"
	"math/rand"
	"strings"
	"time"
)

func init() {
	rand.Seed(time.Now().Unix())
}
func GenByte() byte {
	char := uint8(rand.Intn(94) + 32)
	return char
}

func GenRandomString(n int) string {
	str := make([]byte, n)
	for i := 0; i < n; i++ {
		str[i] = GenByte()
	}
	return string(str)
}

func Stradd(n int, str string) string {
	var returnstr string
	for i := 0; i <= n; i++ {
		returnstr = returnstr + str
	}
	return returnstr
}

func StrSfprint(n int, str string) string {
	var returnstr string
	for i := 0; i <= n; i++ {
		returnstr = fmt.Sprintf("%s%s", returnstr, str)
	}
	return returnstr
}
func StrStringBuilder(n int, str string) string {
	var returnstr strings.Builder
	for i := 0; i <= n; i++ {
		returnstr.WriteString(str)
	}
	return returnstr.String()
}

func StrBytebuffer(n int, str string) string {
	var returnstr bytes.Buffer
	for i := 0; i <= n; i++ {
		returnstr.WriteString(str)
	}
	return returnstr.String()
}
func StrByteArray(n int, str string) string {
	returnstr := make([]byte, (n+1)*len(str))
	lenth := len(str)
	for i := 0; i <= n; i++ {
		for j := 0; j < lenth; j++ {
			returnstr[j+i*lenth] = str[j]
		}
	}
	return string(returnstr)
}
