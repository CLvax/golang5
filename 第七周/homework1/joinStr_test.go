package main

import "testing"

func BenchmarkJoinAdd(b *testing.B) {
	for i := 0; i < b.N; i++ {
		joinAdd(100000)
	}
}

func BenchmarkJoinSprinf(b *testing.B) {
	for i := 0; i < b.N; i++ {
		joinSprinf(100000)
	}
}

func BenchmarkJoinBuffer(b *testing.B) {
	for i := 0; i < b.N; i++ {
		joinBuffer(100000)
	}
}

func BenchmarkJoinByte(b *testing.B) {
	for i := 0; i < b.N; i++ {
		joinByte(100000)
	}
}
