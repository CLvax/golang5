package main

import (
	"bytes"
	"fmt"
	"math/rand"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func randomString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func joinAdd(n int) string {
	var str string
	for i := 0; i < n; i++ {
		randStr := randomString(rand.Intn(10))
		str = str + randStr
	}
	return str
}

func joinSprinf(n int) string {
	var str string
	for i := 0; i < n; i++ {
		randStr := randomString(rand.Intn(10))
		str = fmt.Sprintf("%s%s", str, randStr)
	}
	return str
}

func joinBuffer(n int) string {
	//var str string
	var bufStr bytes.Buffer
	for i := 0; i < n; i++ {
		randStr := randomString(rand.Intn(10))
		//bytes.Buffer{}.String()
		bufStr.WriteString(randStr)
	}
	return bufStr.String()
}

func joinByte(n int) string {
	var byteStr []byte
	//byteStr := make([]byte,0)
	for i := 0; i < n; i++ {
		randStr := randomString(rand.Intn(10))
		tmp := []byte(randStr)
		byteStr = append(byteStr, tmp...)
	}
	return string(byteStr)
}

func main() {

	//rand.Seed(time.Now().UnixNano())
	//
	//log.Println(joinByte(2))
	//log.Println(randomString(10))

}
