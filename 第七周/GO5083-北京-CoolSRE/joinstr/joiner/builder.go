package joiner

import "strings"

//- 使用strings.builder拼接
func StrBuilder(str string) (res string) {
	sb := strings.Builder{}
	for i := 0; i < len(str); i++ {
		sb.WriteString(string(str[i]))
	}
	return sb.String()
}
