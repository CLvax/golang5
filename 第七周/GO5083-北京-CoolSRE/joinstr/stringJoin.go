package joinstr

/*
- 字符串拼接benchmark
- 对比以下几个方法
```go
func xxx(n int ,str string) string{
}
```

- 使用+拼接
- 使用fmt.sprinf拼接
- 使用strings.builder拼接
- 使用bytes.buffer拼接
- 使用[]byte拼接
- 生成基本字符串的函数如下
```shell script
package main

import "math/rand"

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func randomString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}
*/

/*
 go test -benchmem -bench . joinstr
goos: darwin
goarch: amd64
pkg: joinstr
cpu: Intel(R) Core(TM) i5-4258U CPU @ 2.40GHz
BenchmarkStringBuilder-4        10762288               100.9 ns/op             8 B/op          1 allocs/op
BenchmarkStrSprinf-4             1001030              1191 ns/op             184 B/op         18 allocs/op
BenchmarkStrByte-4              42587038                28.43 ns/op            5 B/op          1 allocs/op
BenchmarkStrBytesBuffer-4        6733728               155.3 ns/op            69 B/op          2 allocs/op
PASS
ok      joinstr 6.273s
*/
