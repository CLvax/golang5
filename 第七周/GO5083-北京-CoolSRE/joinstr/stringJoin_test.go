package joinstr

import (
	"joinstr/joiner"
	"math/rand"
	"testing"
	"time"
)

func RandomString(n int) (res string) {
	const (
		alpha = "abcdefghijklmnoepqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	)
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		RandN := rand.Intn(len(alpha))
		res += string(alpha[RandN])
	}
	return res
}

func BenchmarkStringBuilder(b *testing.B) {
	res := RandomString(5)
	for i := 0; i < b.N; i++ {
		joiner.StrBuilder(res)
	}
	// joiner.StrByte(res)
	//fmt.Println(strSprinf(rs))
	//fmt.Println(strBuilder(rs))
	//fmt.Println(strBytesBuffer(rs))
	//fmt.Println(strByte(rs))
}

func BenchmarkStrSprinf(b *testing.B) {
	res := RandomString(5)
	for i := 0; i < b.N; i++ {
		joiner.StrSprinf(res)
	}
}

func BenchmarkStrByte(b *testing.B) {
	res := RandomString(5)
	for i := 0; i < b.N; i++ {
		joiner.StrByte(res)
	}
}

func BenchmarkStrBytesBuffer(b *testing.B) {
	res := RandomString(5)
	for i := 0; i < b.N; i++ {
		joiner.StrBytesBuffer(res)
	}
}
