package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"strings"
	"testing"
	"time"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func randomString(n int) string {
	b := make([]byte, n)
	rand.Seed(time.Now().UnixNano())
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func addstr(n int) string {
	str1 := randomString(n)
	str2 := randomString(n)
	return str1 + str2
}

func fmtsprintstr(n int) string {
	str1 := randomString(n)
	str2 := randomString(n)
	return fmt.Sprintf("%v%v", str1, str2)
}

func stringsbuilderstr(n int) string {
	str1 := randomString(n)
	str2 := randomString(n)
	str := []string{
		str1,
		str2,
	}
	var b strings.Builder
	for _, s := range str {
		fmt.Fprintf(&b, s)
	}
	return b.String()
}

func bytesbuffer(n int) string {
	str1 := randomString(n)
	str2 := randomString(n)
	var buffer bytes.Buffer
	buffer.WriteString(str1)
	buffer.WriteString(str2)
	return buffer.String()
}

func stringsjoin(n int) string {
	str1 := randomString(n)
	str2 := randomString(n)
	var str []string = []string{str1, str2}
	return strings.Join(str, "")

}

func BenchmarkAddstr(b *testing.B) {
	for n := 0; n < b.N; n++ {
		addstr(1000000)
	}
}

// func BenchmarkRandomString(b *testing.B) {
// 	for n := 0; n < b.N; n++ {
// 		randomString(10000)
// 	}
// }

func BenchmarkFmtsprintstr(b *testing.B) {
	for n := 0; n < b.N; n++ {
		fmtsprintstr(1000000)
	}
}

func BenchmarkStringsbuilderstr(b *testing.B) {
	for n := 0; n < b.N; n++ {
		stringsbuilderstr(1000000)
	}
}
func BenchmarkBytesbuffer(b *testing.B) {
	for n := 0; n < b.N; n++ {
		bytesbuffer(1000000)
	}
}

func BenchmarkStringsjoin(b *testing.B) {
	for n := 0; n < b.N; n++ {
		stringsjoin(1000000)
	}
}
