package main

import (
	"fmt"
	"strings"
)

func main() {
	// str1 := bytes.Buffer{}
	str1 := strings.Builder{}
	a := []byte{'A', 'B', 'C'}
	for _, v := range a {
		str1.WriteByte(v)
	}
	fmt.Println(str1.String())
}
