package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"strings"
	"testing"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

// 使用fmt.Sprintf拼接
func Sprintf(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return fmt.Sprintf("%s", b)
}

// 使用+拼接字符串

func Add(n int) string {
	tmp := ""
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
		tmp += string(b[i])
	}
	return tmp
}

// 使用 strings.Builder 拼接
func StringBuilder(n int) string {
	str := strings.Builder{}
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
		str.WriteByte(b[i])
	}
	return str.String()
}

// 使用bytes.Buffer拼接
func BytesAdd(n int) string {
	str := bytes.Buffer{}
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
		str.WriteByte(b[i])
	}
	return str.String()
}

// 使用 []bytes 拼接

func SliceBytesAdd(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

/* 字符串拼接功能性测试
func main() {
	s1 := Sprintf(10)
	fmt.Println(s1)
	s2 := Add(10)
	fmt.Println(s2)
	s3 := StringBuilder(10)
	fmt.Println(s3)
	s4 := BytesAdd(10)
	fmt.Println(s4)
	s5 := SliceBytesAdd(10)
	fmt.Println(s5)
}

*/
// 测试函数
func benchmarkSprintf(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		Sprintf(i)
	}
}

func benchmarkAdd(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		Add(i)
	}
}

func benchmarkStringBuilder(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		StringBuilder(i)
	}
}

func benchmarkBytesAdd(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		BytesAdd(i)
	}
}

func benchmarkSliceBytesAdd(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		SliceBytesAdd(i)
	}
}

func BenchmarkSprintf(b *testing.B) { benchmarkSprintf(1000, b) }

func BenchmarkAdd(b *testing.B) { benchmarkAdd(1000, b) }

func BenchmarkStringBuilder(b *testing.B) { benchmarkStringBuilder(1000, b) }

func BenchmarkBytesAdd(b *testing.B) { benchmarkBytesAdd(1000, b) }

func BenchmarkSliceBytesAdd(b *testing.B) { benchmarkSliceBytesAdd(1000, b) }
