package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"strings"
	"testing"
	"time"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func randomString(n int) string {
	b := make([]byte, n)
	rand.Seed(time.Now().UnixNano())
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

var count int = 2000

func BenchmarkStringConcat(b *testing.B) {
	// 使用+拼接
	var res string
	for n := 0; n < b.N; n++ {
		var str string
		for i := 0; i < count; i++ {
			s := randomString(10)
			str += s
		}
		res = str
		_ = res
		// log.Println(res)
	}

}

func BenchmarkStringSprintf(b *testing.B) {
	//使用fmt.sprinf拼接
	var res string
	for n := 0; n < b.N; n++ {

		// s := fmt.Sprintf("%s%s", res, randomString(10))
		var str string
		for i := 0; i < count; i++ {
			str = fmt.Sprintf("%s%s", str, randomString(10))
		}
		res = str
		_ = res
		// log.Println(res)
	}
}

func BenchmarkStringBuilder(b *testing.B) {
	var res string
	for n := 0; n < b.N; n++ {

		// s := fmt.Sprintf("%s%s", res, randomString(10))
		var builder strings.Builder
		for i := 0; i < count; i++ {
			builder.WriteString(randomString(10))
		}
		res = builder.String()
		_ = res
		// log.Println(res)
	}
}

func BenchmarkBytesBuffer(b *testing.B) {
	//使用bytes.buffer拼接
	var res string
	for n := 0; n < b.N; n++ {
		buff := new(bytes.Buffer)
		for i := 0; i < count; i++ {
			buff.WriteString(randomString(10))
		}
		res = buff.String()
		_ = res
		// log.Println(res)
	}
}

func BenchmarkByte(b *testing.B) {
	//使用[]byte拼接
	var res string
	for n := 0; n < b.N; n++ {
		var b []byte
		for i := 0; i < count; i++ {
			b = append(b, randomString(10)...)
		}
		// log.Println(b)
		res = string(b)
		_ = res
		// log.Println(res)
	}
}

/*
go test -bench=.  -benchmem -run=none                                                                      ✔ | 10:52:05
goos: darwin
goarch: amd64
pkg: mage/day7/homework1
cpu: Intel(R) Core(TM) i7-1068NG7 CPU @ 2.30GHz
BenchmarkStringConcat-8               51          22875973 ns/op        21150367 B/op       5999 allocs/op
BenchmarkStringSprintf-8              49          23560672 ns/op        21339471 B/op      10024 allocs/op
BenchmarkStringBuilder-8              63          18454771 ns/op          149360 B/op       4018 allocs/op
BenchmarkBytesBuffer-8                63          18449806 ns/op          163952 B/op       4011 allocs/op
BenchmarkByte-8                       63          18453775 ns/op          169840 B/op       4019 allocs/op
PASS
ok      mage/day7/homework1     7.083s
*/
