package main

import "testing"

func BenchmarkAddJoin(b *testing.B)  {
	AddJoin(5000,500)
}

func BenchmarkSprinfJoin(b *testing.B)  {
	SprinfJoin(50000,500)
}

func BenchmarkBuilderJoin(b *testing.B)  {
	BuilderJoin(50000,500)
}

func BenchmarkBufferJoin(b *testing.B)  {
	BufferJoin(50000,500)
}

