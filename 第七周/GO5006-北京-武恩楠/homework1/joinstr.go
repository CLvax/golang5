package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"strings"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func randomString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

// 使用+拼接
func AddJoin(x,n int) string {
	var s string
	for i:=0;i<x;i++{
		s=s+randomString(n)
	}
	return s
}
// 使用fmt.sprin拼接
func SprinfJoin(x,n int) string {
	var s string
	for i:=0;i<x;i++{
		s= fmt.Sprint(s,randomString(n))
	}
	return s
}

// 使用strings.builder拼接
func BuilderJoin(x,n int) string {
	var s strings.Builder
	for i:=0;i<x;i++{
		s.WriteString(randomString(n))
	}
	return s.String()
}

// 使用bytes.buffer拼接
func BufferJoin(x,n int) string {
	var s bytes.Buffer
	for i:=0;i<x;i++{
		s.WriteString(randomString(n))
	}
	return s.String()
}


