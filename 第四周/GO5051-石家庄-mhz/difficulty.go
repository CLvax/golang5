package main

// 作业二：
//用interface和map实现一个发布系统，能发布k8s node(jtype字段) 不同的类型的任务
// 可以增量更新：开启新的，停掉旧的
// 例如原有的是a,b,c ，现在的是b,c,d，可以做到：开启d, 停掉a
type Pusher interface {
	PullCode(data string) bool
	PushCode(data string) bool
}
type k8s struct {
	Name string
	Ver string
}
type node struct {
	Name string
	Ver string
}

func (k *k8s) PullCode(data string) bool {
	return true
}
func (k *k8s) PushCode(data string) bool {
	return true
}
func (n *node) PullCode(data string) bool {
	return true
}
func (n *node) PushCode(data string) bool {
	return true
}

var PS = make(map[string]Pusher)
func main() {
 k :=k8s{
	 Name:     "k8s",
	 Ver: "1.1",
 }
 n :=node{
	 Name:     "node",
	 Ver: "1.1",
 }

 PS["k8s"] = &k
 PS["node"] = &n



}