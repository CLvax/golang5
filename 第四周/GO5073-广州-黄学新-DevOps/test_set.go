package main

import (
	"fmt"
	"sort"
)

type Empty struct {}

var empty Empty

type Set struct {
	m map[interface{}] Empty
}

func SetFactory() * Set {
	return &Set{
		m: map[interface{}]Empty{},
	}
}

// 添加元素
func (s * Set) Add (val interface{}) {
	s.m[val] = empty
}

// 删除元素
func (s * Set) Del(val interface{}) {
	delete(s.m, val)
}

//清空set
func (s * Set) Clear() {
	s.m = make(map[interface{}] Empty, 0)
}

// 查看一个元素是否存在
func (s * Set) Exists(val interface{}) bool {
	_, ok := s.m[val]
	return ok
}

//遍历Set
func (s * Set) Traverse() {
	intVals := make([]int, 0)
	strVals := make([]string, 0)
	var t interface{}
	fmt.Println(s)
	for v := range s.m {
		t = v
		switch t.(type) {
		case string:
			strVals = append(strVals, t.(string))
		case int:
			intVals = append(intVals, t.(int))
		}
	}
	sort.Ints(intVals)
	sort.Strings(strVals)

	for _, v := range intVals {
		fmt.Println(v)
	}
	for _, v := range strVals {
		fmt.Println(v)
	}
}

func (s * Set) Len() int {
	return len(s.m)
}

func main() {
	s := SetFactory()

	for i := 0; i < 100; i++ {
		s.Add("hello Go Set")
		s.Add("hello Go Set111")
		s.Add("hello Go Set2222")
		s.Add("hello Go Set3333")
		s.Add("hello Go Set4444")
		s.Add(i)
	}

	s.Del("hello Go Set4444")
	s.Del("hello Go Set")
	k := s.Exists(3)
	fmt.Println(k)

	s.Traverse()
}
// 可以考虑详细些