package main

import (
	"fmt"
	"sync"
	"time"
)

func NewSet() *Set {
	return &Set{
		m: map[string]int{},
	}
}

type Set struct {
	m map[string]int
	sync.RWMutex
	sync.WaitGroup
}

func (s *Set) Add(str string) {
	s.Lock()
	defer s.Unlock()
	if value, ok := s.m[str]; ok {
		s.m[str] = value + 1
	} else {
		s.m[str] = 1
	}
}

func (s *Set) Del(str string) {
	s.Lock()
	defer s.Unlock()
	if value, ok := s.m[str]; ok {
		if value == 1 {
			delete(s.m, str)
		} else {
			s.m[str] = value - 1
		}
		return
	}
	fmt.Printf("%s 不存在,删除失败！", str)
}

func (s *Set) Get(str string) {
	s.RLock()
	defer s.Unlock()
	if value, ok := s.m[str]; ok {
		fmt.Printf("内容: %s, 重复次数: %d\n", str, value)
		return
	}
	fmt.Printf("该「%s」参数未找到", str)
}

func (s *Set) Merge(set *Set) {
	s.Lock()
	defer s.Unlock()
	if set.m != nil {
		for k, _ := range set.m {
			if value, ok := s.m[k]; ok {
				s.m[k] = value + 1
			} else {
				s.m[k] = 1
			}
		}
		return
	}
	fmt.Printf("查询不到需要合并的Set方法")
}

func (s *Set) PrintElement() {
	s.RLock()
	defer s.RUnlock()
	fmt.Println("内容如下:")
	for k, v := range s.m {
		fmt.Printf("内容: %2s, 重复次数: %2d\n", k, v)
	}
}

// 检查输入的string是否已经存在
func (s *Set) JudgeElement(str string) bool {
	s.RLock()
	defer s.RUnlock()
	if value, ok := s.m[str]; ok {
		fmt.Printf("%s 存在的数量: %d", str, value)
		return true
	}
	fmt.Printf("%s 不存在！", str)
	return false
}

func main() {
	b := NewSet()
	b.Add("my B 1")
	b.Add("my B 2")
	a := NewSet()
	a.Add("hello")
	a.Add("world")
	a.Add("ma")
	a.Add("ge")
	a.Add("ge")
	go a.Add("ge")
	go a.Add("ge")
	go a.Add("ge")
	go a.Add("ge")
	a.Del("ge")
	a.Merge(b)
	a.JudgeElement("hello")

	time.Sleep(10 * time.Second)
	a.PrintElement()
}
// 初始化时加入参数怎么处理？