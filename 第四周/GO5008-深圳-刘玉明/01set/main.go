// 作业简单的题:实现一个线程安全的集合set，元素是string
// - 要求有 NewSet方法初始化
// - Add方法添加元素，添加重复元素可以去重
// - Del方法删除元素
// - Merge方法合并另一个set
package main

import (
	"fmt"
	"sync"
)

type Set struct {
	m map[string]string
	sync.RWMutex
}

func Newset() *Set {
	return &Set{
		m: map[string]string{},
	}
}

func (s *Set) Add(k string) {
	s.Lock()
	defer s.Unlock()
	v2, ok := s.m[k]
	if ok {
		fmt.Printf("该key:%v 在map中已存在\n", v2)
	} else {
		s.m[k] = k
	}
	fmt.Println(s.m)
}

func (s *Set) Del(k string) {
	s.Lock()
	defer s.Unlock()
	delete(s.m, k)

}
func (s *Set) Merage(m map[string]string) {
	s.Lock()
	defer s.Unlock()
	for k1, _ := range m {
		v2, ok := s.m[k1]
		if ok {
			fmt.Printf("该key:%v 在map中已存在\n", v2)
		} else {
			s.m[k1] = k1
		}
	}
	fmt.Println(s.m)
}

func main() {
	var Setmap = Newset()
	fmt.Println(Setmap.m)
	Setmap.Add("abc")
	Setmap.Add("abcd")
	// fmt.Println(Setmap.m)
	Setmap.Del("abcd")
	fmt.Println(Setmap.m)
	userInfo := map[string]string{
		"username": "username",
		"abc":      "abc",
		"abcd":     "abcd",
		"abcde":    "abcde",
	}
	Setmap.Merage(userInfo)
	fmt.Println(Setmap.m)

}
// 当初始化时传入参数怎么处理？