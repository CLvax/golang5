package main

import "fmt"

/**
 * @Classname p2
 * @Description TODO
 * @author cjf
 * @Date 2021/6/8 9:42
 * @Version V1.0
 */

/*
写一个单位转换的函数, 同学只需要把函数逻辑部分编写完成,
测试方法: main 函数内部 通过HumanBytesLoaded(1024)调用
*/

const (
	_          = iota
	KB float64 = 1 << (10 * iota) // 2^10
	MB
	GB
	TB
	EB
)

// HumanBytesLoaded 单位转换(B KB MB GB TB EB)
func HumanBytesLoaded(bytesLength float64) string {
	var resp string

	// bytesLength
	fmt.Printf("需要转换的值: %fB\n", bytesLength)

	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
	switch {
	case bytesLength >= EB:
		resp = fmt.Sprintf("%.2fEB", bytesLength/EB)
		return resp
	case bytesLength >= TB:
		resp = fmt.Sprintf("%.2fTB", bytesLength/TB)
		return resp
	case bytesLength >= GB:
		resp = fmt.Sprintf("%.2fGB", bytesLength/GB)
		return resp
	case bytesLength >= MB:
		resp = fmt.Sprintf("%.2fMB", bytesLength/MB)
		return resp
	case bytesLength >= KB:
		resp = fmt.Sprintf("%.2fKB", bytesLength/KB)
		return resp
	}

	// 返回结果
	resp = fmt.Sprintf("%.2fB", bytesLength)
	return resp
}

func main() {
	fmt.Println(HumanBytesLoaded(2 * 1024))
}

//参数由用户输入试下
