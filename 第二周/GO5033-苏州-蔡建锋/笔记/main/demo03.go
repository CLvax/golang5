package main

/**
 * @Classname demo06
 * @Description TODO
 * @author cjf
 * @Date 2021/6/5 17:19
 * @Version V1.0
 */

import "fmt"

func main() {
	for i := 1; i <= 10; i++ {
		for k := 1; k <= 10; k++ {
			fmt.Print(k)
			fmt.Print("---")
		}
		fmt.Print("---")
		fmt.Print(i)
		fmt.Print("\n")
	}
}
