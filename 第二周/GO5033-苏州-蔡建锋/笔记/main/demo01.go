package main

/**
 * @Classname demo06
 * @Description TODO
 * @author cjf
 * @Date 2021/6/5 17:19
 * @Version V1.0
 */

import "fmt"

func main() {
	a := 30
	switch a {
	case 10:
		fmt.Print("case1")
		fallthrough
	case 20:
		fmt.Print("case2")
		fallthrough
	case 30:
		fmt.Print("case3")
		fallthrough
	default:
		fmt.Print("default")
	}

	//多条件匹配

}
