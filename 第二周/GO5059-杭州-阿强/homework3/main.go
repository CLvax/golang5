package main

import (
	"fmt"
	"math/rand"
	"time"
)

var inputNum int

func main() {

	// 随机生成两个数字用于计算题
	rand.Seed(time.Now().Unix()) //产生Seed
	a1 := rand.Intn(10)
	a2 := rand.Intn(10)
	// 随机生成猜题机会
	guessCount := rand.Intn(10)

	fmt.Printf("请回答如下问题: %d * %d = : ", a1, a2)
	fmt.Scanf("%d", &inputNum)
	if a1*a2 == inputNum {
		fmt.Printf("回答正确,拥有%d次猜题的机会\n", guessCount)
		// 随机生成中奖数值,中奖率为万分之一
		luckNum := rand.Intn(10000)
		for i := 1; i <= guessCount; i++ {
			// 随机生成猜奖数值
			guessLuckNum := rand.Intn(10000)
			if luckNum == guessLuckNum {
				fmt.Printf("当前是你第%d次抽奖: 抽奖结果 已中奖\n", i)
				return
			} else {
				fmt.Printf("当前是你第%d次抽奖: 抽奖结果 未中奖\n", i)
			}
		}

	} else {
		fmt.Printf("回答错误,请下次再来\n")
		return
	}

}
