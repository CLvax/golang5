package homework2

import (
	"fmt"
	"testing"
)

const (
	B = 1 << (10 * iota)
	KB
	MB
	GB
	TB
	EB
)

// HumanBytesLoaded 单位转换(B KB MB GB TB EB)
func HumanBytesLoaded(bytesLength int64) string {
	// bytesLength
	fmt.Printf("需要转换的值: %dB", bytesLength)

	fmt.Println(KB, MB, GB, TB, EB)
	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
	// 返回转换的结果

	switch {
	case bytesLength >= EB:
		return fmt.Sprintf("%.3f%s", float64(bytesLength/EB), "EB")
	case bytesLength >= TB:
		return fmt.Sprintf("%.3f%s", float64(bytesLength/TB), "TB")
	case bytesLength >= GB:
		return fmt.Sprintf("%.3f%s", float64(bytesLength/GB), "GB")
	case bytesLength >= MB:
		return fmt.Sprintf("%.3f%s", float64(bytesLength/MB), "MB")
	case bytesLength >= KB:
		return fmt.Sprintf("%.3f%s", float64(bytesLength/KB), "KB")
	default:
		return fmt.Sprintf("%d", bytesLength)
	}

}

func TestLength(t *testing.T) {
	a := HumanBytesLoaded(1024 * 1024 * 1024)
	fmt.Println(a)
}
