package main

import (
	"bufio"
	"fmt"
	"os"
)

func GetBytes() (item int, getBytes uint) {
	var inputBytes uint

	fmt.Println("请输入要转换的bytes大小:")
	stdin := bufio.NewReader(os.Stdin)

	line, _, err := stdin.ReadLine()
	if err != nil {
		panic(err)
	}

	n, err := fmt.Sscanln(string(line), &inputBytes)
	if err != nil {
		panic(err)
	}

	return n, inputBytes
}

func main() {
	const (
		B  = 1024
		KB = B * 1024
		MB = KB * 1024
		GB = MB * 1024
		EB = GB * 1024
	)
	_, srcBytes := GetBytes()

	fmt.Println("要转换的字节", srcBytes)
	if srcBytes < B {
		fmt.Printf("%dBytes", srcBytes)
		fmt.Println()
	} else if srcBytes >= B && srcBytes < KB {
		fmt.Printf("%.2fKB", float32(srcBytes/B))
		fmt.Println()
	} else if srcBytes >= KB && srcBytes < MB {
		fmt.Printf("%.2fMB", float32(srcBytes/KB))
		fmt.Println()
	} else if srcBytes >= MB && srcBytes < GB {
		fmt.Printf("%.2fGB", float32(srcBytes/MB))
		fmt.Println()
	} else {
		fmt.Printf("%.2fEB", float32(srcBytes/GB))
		fmt.Println()
	}
}

// 功能实现的不错，不过可以把判断逻辑放到GetBytes，main函数可以仅实现调用
