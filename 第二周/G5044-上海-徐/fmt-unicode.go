package main

import "fmt"

func main() {

	binary := [...]uint{
		0b1000100001100011,
		0b0110011100001101,
		0b0101010110011100,
		0b0110101100100010,
		0b0111101001111111,
		0b0100111000101101,
		0b0101011011111101,
		0b0111111010100010}

	for _, v := range binary {
		fmt.Printf("二进制: %b, Unicode: %U,  字符%c", v, v, v)
		fmt.Println()
	}
}

// 可以想下，使用Printf时换行怎么实现
