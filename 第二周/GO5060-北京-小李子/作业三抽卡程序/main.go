package main

import (
	"fmt"
	"math/rand"
	"time"
)

/*
做一个抽卡程序, 中奖率万分之一, 看看你多少个10连能中奖
请回答如下问题, 比如: a + b =:  m
恭喜你 回答正确，随机获取n[1~10]的抽奖机会
当前是你第1次抽奖: 抽奖结果 未中奖
当前是你第2次抽奖. 抽奖结果 未中奖
当前是你第3次抽奖. 抽奖结果 未中奖
...
*/

/*
我的理解：
	1、随机生成 a b 并显示页面让用户计算 a + b的值
	2、得到结果后，进行存储
	3、随机1-10生成一个数字并显示出来做为抽奖机会
	4、在1万个值中进行随机生成一个数与结果值进行比对，如果相等则等同于中奖，否则进行下一次机会，直到用完抽奖机会
*/

func main() {
	/*
		运行方式: go run demo00.go
	*/
	r := rand.New(rand.NewSource(time.Now().Unix()))
	index := 10000
	var (
		a     int
		b     int
		n     int
		yn    string
		value int
	)

	for {
		a = r.Intn(index / 100)
		b = r.Intn(index)
		fmt.Printf("请回答如下随机问题:  %d + %d = ", a, b)
		fmt.Scanln(&value)
		if (a + b) != value {
			continue
		}
		n = r.Intn(9)
		fmt.Printf("恭喜你 回答正确，随机获取[1~10]的抽奖机会: 已获取%d 机会\n", n+1)
		fmt.Printf("兑奖值: %d\n", value)
		for i := 1; i <= n+1; i++ {
			cj := r.Intn(index)
			fmt.Printf("猜奖值: %d\n", cj)
			time.Sleep(time.Second * 2)
			if cj == value {
				fmt.Printf("当前是你第%d次抽奖. 抽奖结果 已中奖！\n", i)
				return
			} else {
				fmt.Printf("当前是你第%d次抽奖. 抽奖结果 未中奖！\n", i)
			}
		}
		fmt.Print("已结束！是否继续? : ")
		fmt.Scanln(&yn)
		if yn != "y" && yn != "Y" {
			return
		}
	}
}
