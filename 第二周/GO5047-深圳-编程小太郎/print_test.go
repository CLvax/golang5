package print_test

import (
	"fmt"
	"strconv"
	"strings"
	"testing"
)

func TestPrint(t *testing.T) {

	str := `1000 1000 0110 0011
0110 0111 0000 1101
0101 0101 1001 1100
0110 1011 0010 0010
0111 1010 0111 1111
0100 1110 0010 1101
0101 0110 1111 1101
0111 1110 1010 0010
`
	//fmt.Println(strings.Split(str, "\n"))

	// 对原始字符串通过\n分隔符进行切割后通过for循环遍历出一维数组
	for _, i := range strings.Split(str, "\n") {
		// 将空格进行去除替换
		//fmt.Println(strings.Replace(i, " ", "", -1))
		toBin := strings.Replace(i, " ", "", -1)
		// 将二进制转换成无符号十六进制整数
		//fmt.Println(strconv.ParseUint(toBin, 2, 0))
		/*
			bitSize参数表示转换为什么位的int/uint，有效值为0、8、16、32、64,当bitSize=0的时候,表示转换为int或uint类型.
			base参数表示以什么进制的方式去解析给定的字符串，有效值为0、2-36.
			当base=0的时候,表示根据string的前缀来判断以什么进制去解析:
			0x开头的以16进制的方式去解析,0开头的以8进制方式去解析,其它的以10进制方式解析.
		*/
		toWord, _ := strconv.ParseUint(toBin, 2, 0)
		// 格式化打印整数对应的字符
		//fmt.Printf("%c\n", toWord)
		fmt.Printf("%c", toWord)
	}
}
