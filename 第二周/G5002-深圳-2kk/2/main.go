package main

import "fmt"

// HumanBytesLoaded 单位转换(B KB MB GB TB EB)
func HumanBytesLoaded(bytesLength int64) string {
    var resp string

    // bytesLength
    fmt.Printf("需要转换的值: %dB", bytesLength)

    // 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
    if bytesLength < 1024{
        resp = fmt.Sprintf("%.2fB\n", float64(bytesLength)/float64(1))
    } else if bytesLength < ( 1024 * 1024 ){
        resp = fmt.Sprintf("%.2fKB\n", float64(bytesLength)/float64(1024))
    } else if bytesLength < ( 1024 * 1024 * 1024 ){
        resp = fmt.Sprintf("%.2fMB\n", float64(bytesLength)/float64(1024*1024))
    } else if bytesLength < ( 1024 * 1024 * 1024 * 1024){
        resp = fmt.Sprintf("%.2fGB\n", float64(bytesLength)/float64(1024*1024*1024))
    } else if bytesLength < (1024 * 1024 * 1024 * 1024 * 1024){
        resp = fmt.Sprintf("%.2fTB\n", float64(bytesLength)/float64(1024*1024*1024*1024))
    } else {
        resp = fmt.Sprintf("%.2fEB\n", float64(bytesLength)/float64(1024*1024*1024*1024*1024))
    }
    // 返回转换的结果
    return resp
}



func main()  {
	ret := HumanBytesLoaded(1024)
    fmt.Println("转换后的值:", ret)
	ret = HumanBytesLoaded(20480)
    fmt.Println("转换后的值:", ret)
}