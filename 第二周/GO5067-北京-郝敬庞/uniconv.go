package main

import (
	"fmt"
)

// 写一个单位转换的函数, 同学只需要把函数逻辑部分编写完成
// 测试方法: main 函数内部 通过HumanBytesLoaded(1024)调用

// HumanBytesLoaded 单位转换(B KB MB GB TB EB)
func HumanBytesLoaded(bytesLength int64) string {
	var resp string

	// bytesLength
	fmt.Printf("需要转换的值: %dB\n", bytesLength)

	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
	if bytesLength > 1<<10 && bytesLength < 1<<20 {
		//resp = strconv.FormatInt(int64(bytesLength/1024),10)+"KB"
		resp = fmt.Sprintf("%.2f", float32(bytesLength)/float32(1<<10)) + "KB"
	} else if bytesLength >= 1<<20 && bytesLength < 1<<30 {
		resp = fmt.Sprintf("%.2f", float32(bytesLength)/float32(1<<20)) + "MB"
	} else if bytesLength >= 1<<30 && bytesLength < 1<<40 {
		resp = fmt.Sprintf("%.2f", float32(bytesLength)/float32(1<<30)) + "GB"
	} else if bytesLength >= 1<<40 && bytesLength < 1<<50 {
		resp = fmt.Sprintf("%.2f", float32(bytesLength)/float32(1<<40)) + "TB"
	} else if bytesLength >= 1<<50 && bytesLength < 1<<60 {
		resp = fmt.Sprintf("%.2f", float32(bytesLength)/float32(1<<50)) + "EB"
	} else {
		resp = fmt.Sprintf("%v", bytesLength) + "B"
	}
	return resp
}

func main() {
	c := HumanBytesLoaded(102400000099918)
	fmt.Println(c)
}
