package main

import (
	"fmt"
)

func main() {
	var B float32
	B = 1
	KB := B * 1024
	MB := KB * 1024
	GB := MB * 1024
	TB := GB * 1024
	PB := TB * 1024

	var a1 float32
	fmt.Println("请输入字节数：")
	fmt.Scanln(&a1)
	if a1 < 0 || a1 > (PB*1024) {
		fmt.Println("输入错误")
	} else if a1 >= 0 && a1 < 1024 {
		fmt.Println("字节大小为：", a1, "B")
	} else if a1 >= KB && a1 < MB {
		fmt.Printf("字节大小为：%.2f KB", a1/KB)
	} else if a1 >= MB && a1 < GB {
		fmt.Printf("字节大小为：%.2f MB", a1/MB)
	} else if a1 >= GB && a1 < TB {
		fmt.Printf("字节大小为：%.2f MB", a1/GB)
	} else if a1 >= TB && a1 < PB {
		fmt.Printf("字节大小为：%.2f MB", a1/TB)
	} else if a1 >= PB && a1 < (PB*1024) {
		fmt.Printf("字节大小为：%.2f MB", a1/PB)
	} else {
		fmt.Println("too large!")
	}

}
