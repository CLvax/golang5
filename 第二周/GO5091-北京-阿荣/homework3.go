package main

import (
	"fmt"
	"math/rand"
	"time"
)

/*
3. 做一个抽卡程序, 中奖率万分之一, 看看你多少个10连能中奖
```
请回答如下问题, 比如: a + b =:  m
恭喜你 回答正确，随机获取n[1~10]的抽奖机会
当前是你第1次抽奖: 抽奖结果 未中奖
当前是你第2次抽奖. 抽奖结果 未中奖
当前是你第3次抽奖. 抽奖结果 未中奖
...
```
*/

func main() {
	for {
		seconds := time.Now().Unix()
		rand.Seed(seconds)
		// 产生随机数
		a := rand.Intn(10)
		b := rand.Intn(10)
		m := a + b
		var n int
		// 中奖条件
		const BingoNumber int = 5
		fmt.Printf("%d + %d =?, 回答正确可以获得抽奖机会:", a, b)
		fmt.Scanln(&n)
		if n != m {
			fmt.Println("很遗憾，回答错误")
			continue
		} else {
			chance := rand.Intn(10) + 1
			fmt.Printf("恭喜你 回答正确，随机获取了%d次抽奖机会\n", chance)
			for i := 1; i <= chance; i++ {
				var r string
				n := rand.Intn(10)
				if n == BingoNumber {
					r = "中奖了"
					//break
				} else {
					r = "未中奖"
				}
				fmt.Printf("当前是你第%d次抽奖: 抽奖结果 %s\n", i, r)
			}
		}
	}

}

