package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
END:
	fmt.Println("抽奖开始：当前有10抽奖机会")
	fmt.Println("请输入中奖号码")
	rand.Seed(time.Now().Unix())
	var random int = rand.Intn(100) //设置随机数种子（设置一次即可）
	//fmt.Printf("奖号为%d,用于验证程序是否运行正常,验证通过即可关闭此行代码\n", random) //
	sum := 0
	for {
		sum++
		fmt.Printf("第 %d 次抽奖,请输入你心中的特码?\n", sum)
		var scroe int
		fmt.Scan(&scroe)
		if random != scroe {
			if sum == 10 {
				fmt.Printf("当前是你第 %d 次猜的特码，结果： 未中奖!!很抱歉没有机会了\n", sum)
			} else if sum == 9 {
				fmt.Printf("当前是你第 %d 次猜的特码， 结果： 未中奖!!还剩最后 %d 次机会了\n", sum, 10-sum)
			} else {
				fmt.Printf("当前是你第 %d 次猜的特码， 结果： 未中奖!!还剩余 %d 次机会哦!\n", sum, 10-sum)
			}

		} else {
			fmt.Println("^_^恭喜你,中六合彩了")
			break
		}
		if sum == 10 {
			fmt.Println("要继续抽奖吗？")
			fmt.Print("请输入：(y/n)")
			var newScroe string
			fmt.Scan(&newScroe)
			if newScroe == "y" {
				fmt.Println(" (^_^) 上局大奖号码为：", random)
				//sum = 0
				//rand.Seed(time.Now().Unix())
				//random = rand.Intn(100) //设置随机数种子（设置一次即可）
				//continue
				goto END

			} else {
				fmt.Println(" (>_<) 抽奖结束！！！")
				fmt.Println("大奖号码为：", random)
				break
			}

		}
	}
}
