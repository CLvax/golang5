package humanBytesLoaded

import "fmt"

const (
	KB = 1024 << (iota * 10)
	MB
	GB
	TB
	EB
)

func HumanBytesLoaded(bytesLength int64) string {
	var resp string
	var unit string
	// bytesLength
	bytesLengthf := float64(bytesLength)
	fmt.Printf("需要转换的值: %fB \n", bytesLengthf)
	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
	fmt.Println(bytesLength, KB)
	if bytesLength < KB {
		bytesLengthf /= 1024
		unit = "B"
	}
	if bytesLength >= KB {
		bytesLengthf /= 1024
		unit = "KB"
	}

	if bytesLength >= MB {
		bytesLengthf /= 1024
		unit = "MB"
	}

	if bytesLength >= GB {
		bytesLengthf /= 1024
		unit = "GB"
	}

	if bytesLength >= TB {
		bytesLengthf /= 1024
		unit = "TB"
	}

	if bytesLength >= EB {
		bytesLengthf /= 1024
		unit = "EB"
	}
	resp = fmt.Sprintf("%2.3f%s", bytesLengthf, unit)
	// 返回转换的结果
	return resp
}


//实现的不错，知识内容用的也相对比较多