package humanBytesLoaded

import (
	"testing"
)

func TestHumanBytesLoaded(t *testing.T) {
	n := []string{
		"1.000KB",
		"1.000MB",
		"1.000GB",
	}

	n1 := []int64{
		1024,
		1048576,
		1073741824,
	}
	for i := 0; i < 3; i++ {
		s := HumanBytesLoaded(n1[i])
		if s != n[i] {
			t.Log("Error: ", s, n[i])
		}
	}

}
