package main

import (
	"fmt"
	"math/rand"
	"time"
)

// 写一个单位转换的函数, 同学只需要把函数逻辑部分编写完成,
// 测试方法: main 函数内部 通过HumanBytesLoaded(1024)调用
// HumanBytesLoaded 单位转换(B KB MB GB TB EB)

func main() {
	rand.Seed(time.Now().UnixNano())
	num := rand.Intn(100)
	npond := rand.Perm(100)
	for {
		var ans int
		a, b := rand.Intn(10), rand.Intn(10)
		fmt.Printf("请回答如下问题 %d + %d = ", a, b)
		fmt.Scan(&ans)
		if ans != a+b {
			fmt.Println("回答错误退出游戏!!!")
			break
		}

		for i := 1; i <= 10; i++ {
			if len(npond) < 0 {
				fmt.Println("奖池已经抽光了!!!")
				break
			}

			if npond[0] == num {
				fmt.Printf("第%d次抽奖%d，恭喜中奖!!! \n", i, npond[0])
				return
			} else {
				fmt.Printf("第%d次抽奖%d，并未中奖!!! \n", i, npond[0])
			}

			npond = npond[1:]
		}
	}
}
