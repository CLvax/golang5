package cvEncoding

import (
	"fmt"
	"testing"
)

func TestBinary(t *testing.T) {
	Binary()
}

func TestOctal(t *testing.T) {
	Octal()
}

func TestHexadecimal(t *testing.T) {
	Hexadecimal()
}

func TestBcvD(t *testing.T) {
	strs := []string{
		"1000100001100011",
		"0110011100001101",
		"0101010110011100",
		"0110101100100010",
		"0111101001111111",
		"0100111000101101",
		"0101011011111101",
		"0111111010100010",
	}
	ns := []int{
		34915,
		26381,
		21916,
		27426,
		31359,
		20013,
		22269,
		32418,
	}
	ints := BcvD(strs)
	for k, v := range ints {
		if v != ns[k] {
			t.Logf("Error: %d is not eq %d \n", v, ns[k])
		}
		fmt.Printf("%c", v)
	}
}

func Test_conversion(t *testing.T) {
	conversion()
}
