package lottery

import (
	"fmt"
	"math/rand"
	"time"
)

func Lottery() {
	rand.Seed(time.Now().UnixNano())
	num := rand.Intn(100)
	npond := rand.Perm(100)
	for {
		var ans int
		a, b := rand.Intn(10), rand.Intn(10)
		fmt.Printf("请回答如下问题 %d + %d = ", a, b)
		fmt.Scan(&ans)
		if ans != a+b {
			fmt.Println("回答错误退出游戏!!!")
			break
		}

		for i := 1; i <= 10; i++ {
			if len(npond) < 0 {
				fmt.Println("奖池已经抽光了!!!")
				break
			}

			if npond[0] == num {
				fmt.Printf("第%d次抽奖%d，恭喜中奖!!! \n", i, npond[0])
				return
			} else {
				fmt.Printf("第%d次抽奖%d，并未中奖!!! \n", i, npond[0])
			}

			npond = npond[1:]
		}
	}
}

// 未实现抽奖逻辑
