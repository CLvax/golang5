package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"
)

// 做一个抽卡程序, 中奖率万分之一, 看看你多少个10连能中奖
// 请回答如下问题, 比如: a + b =:  m
// 恭喜你 回答正确，随机获取n[1~10]的抽奖机会
// 当前是你第1次抽奖: 抽奖结果 未中奖
// 当前是你第2次抽奖. 抽奖结果 未中奖
// 当前是你第3次抽奖. 抽奖结果 未中奖
// ...

func main() {
	var ResultNum, GuessNum int
	rand.Seed(time.Now().Unix())

	// 随机生成验证数字
	x := rand.Intn(50) + 1
	y := rand.Intn(50) + 1

	// 随机生成中奖数字
	LuckNum := rand.Intn(10000) + 1
	//fmt.Println(LuckNum)
	fmt.Printf("请回答如下问题\n%d + %d = ", x, y)
	fmt.Scanln(&ResultNum)
	if x+y == ResultNum {
		z := rand.Intn(10) + 1
		fmt.Printf("恭喜你，回答正确，获得%d次抽奖机会\n", z)
		for i := 1; i <= z; i++ {

			fmt.Printf("当前是你第 %d 次抽奖，请输入数字: ", i)
			fmt.Scanln(&GuessNum)
			if GuessNum == LuckNum {
				fmt.Printf("恭喜您，中奖了!!!\n程序即将退出\n")
				time.Sleep(time.Second * 2)
				break
			}
			fmt.Println("未中奖")
		}
	} else {
		fmt.Println("回答错误，程序即将退出!!!")
		time.Sleep(time.Second * 2)
		os.Exit(1)
	}
}

// 完成的不错
