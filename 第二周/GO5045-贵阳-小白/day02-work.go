package main

import (
	"fmt"
	"strconv"
	"math/rand"
	"time"
)

/* 1.请将这段二进制翻译成中文(unicode编码),并打印出翻译过程(比如 二进制: 1000 1000 0110 0011, unicode: U+8863, 字符: 衣)
1000 1000 0110 0011
0110 0111 0000 1101
0101 0101 1001 1100
0110 1011 0010 0010
0111 1010 0111 1111
0100 1110 0010 1101
0101 0110 1111 1101
0111 1110 1010 0010 */
func work01() {
	fmt.Println("################################################ 第一题开始 ######################################")
	array := []int{
		0b1000100001100011,
		0b0110011100001101,
		0b0101010110011100,
		0b0110101100100010,
		0b0111101001111111,
		0b0100111000101101,
		0b0101011011111101,
		0b0111111010100010,
	}
	for _, element := range array{
		fmt.Printf("二进制: %-16b, unicode码: %-6U, unicode： %c\n", element, element, element)
	}
	fmt.Println("################################################ 第一题结束 ######################################\n\n")	
}


/* 2.写一个单位转换的函数, 同学只需要把函数逻辑部分编写完成,
     测试方法: main 函数内部 通过HumanBytesLoaded(1024)调用
     HumanBytesLoaded 单位转换(B KB MB GB TB EB) */	 
func HumanBytesLoaded(bytesLength int64) string {
	var resp string
	const (
		_ = iota
		KB = 1 << (10 * iota)
		MB = 1 << (10 * iota)
		GB = 1 << (10 * iota)
		TB = 1 << (10 * iota)
		EB = 1 << (10 * iota)
	)
	fmt.Println("################################################ 第二题开始 ######################################")
	// bytesLength
	fmt.Printf("需要转换的值: %dB\n", bytesLength)

	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
	
	if bytesLength >= EB {
		value := float64(bytesLength) / EB
		resp = strconv.FormatFloat(value, 'f', -1, 64)	+ "EB"
	} else if bytesLength >= TB {
		value := float64(bytesLength) / TB
		resp = strconv.FormatFloat(value, 'f', -1, 64) + "TB"
	} else if bytesLength >= GB {
		value := float64(bytesLength) / GB
		resp = strconv.FormatFloat(value, 'f', -1, 64) + "GB"
	} else if bytesLength >= MB {
		value := float64(bytesLength) / MB
		resp = strconv.FormatFloat(value, 'f', -1, 64) + "MB"
	} else {
		value := float64(bytesLength) / KB
		resp = strconv.FormatFloat(value, 'f', -1, 64) + "KB"
	}	
	// 返回转换的结果
	return resp
}

/* 3.做一个抽卡程序, 中奖率万分之一, 看看你多少个10连能中奖
	 请回答如下问题, 比如: a + b =:  m
	 恭喜你 回答正确，随机获取n[1~10]的抽奖机会
	 当前是你第1次抽奖: 抽奖结果 未中奖  
	 当前是你第2次抽奖. 抽奖结果 未中奖
	 当前是你第3次抽奖. 抽奖结果 未中奖
	 ... */
func work03() {
	// 生成随机数种子
	rand.Seed(time.Now().Unix())
	num1 := rand.Intn(10)
	num2 := rand.Intn(10)
	var sum int
	// 生成抽奖次数
	n := rand.Intn(10)
	// 生成中奖数字
	successNum := rand.Intn(9989)
	
	fmt.Println("请回答如下问题, 比如: a + b =:  m")
	fmt.Printf("%v + %v = ", num1, num2)
	fmt.Scan(&sum)
	if sum == num1 + num2 {
		if n == 0 {
			n = n + 1
		}
		fmt.Println("恭喜你 回答正确，随机获取n[1~10]的抽奖机会")
		for i := 1; i <= n; i++ {
			if num1 == successNum {
				fmt.Printf("当前是你第%v次抽奖: 抽奖结果 中奖\n", i)
			} else {
				fmt.Printf("当前是你第%v次抽奖: 抽奖结果 未中奖\n", i)
			}
		}
	} else {
		fmt.Println("回答错误")

	}
}

func main() {
	work01()
	value := HumanBytesLoaded(285873023221800000)
	fmt.Println(value)
	fmt.Println("################################################ 第二题结束 ######################################\n\n")
	work03()
}
