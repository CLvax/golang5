package main

import (
	"fmt"
)

func HumanBytesLoaded(bytesLength int64) string {
	var resp string

	// bytesLength
	fmt.Printf("需要转换的值: %dB \n", bytesLength)

	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
	if bytesLength < 1024 {
		//resp = strconv.FormatInt(bytesLength, 10) + "B"
		resp = fmt.Sprintf("%.2fB", float64(bytesLength)/float64(1))
	} else if bytesLength < (1024 * 1024) {
		resp = fmt.Sprintf("%.2fKB", float64(bytesLength)/float64(1024))
	} else if bytesLength < (1024 * 1024 * 1024) {
		resp = fmt.Sprintf("%.2fMB", float64(bytesLength)/float64(1024*1024))
	} else if bytesLength < (1024 * 1024 * 1024 * 1024) {
		resp = fmt.Sprintf("%.2fGB", float64(bytesLength)/float64(1024*1024*1024))
	} else if bytesLength < (1024 * 1024 * 1024 * 1024 * 1024) {
		resp = fmt.Sprintf("%.2fTB", float64(bytesLength)/float64(1024*1024*1024*1024))
	} else {
		resp = fmt.Sprintf("%.2fEB", float64(bytesLength)/float64(1024*1024*1024*1024*1024))
	}

	// 返回转换的结果
	return resp
}

func main() {
	res := HumanBytesLoaded(10240000000)
	fmt.Printf(res)
}

// 输出的时候，为方便查看可以加上换行符
