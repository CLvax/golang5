package main

import (
	"fmt"
	"math/rand"
	"time"
)

// '''
// 做一个抽卡程序, 中奖率万分之一, 看看你多少个10连能中奖
// 请回答如下问题, 比如: a + b =:  m
// 恭喜你 回答正确，随机获取n[1~10]的抽奖机会
// 当前是你第1次抽奖: 抽奖结果 未中奖
// 当前是你第2次抽奖. 抽奖结果 未中奖
// 当前是你第3次抽奖. 抽奖结果 未中奖
// '''

func luckDraw(x, y, z int) {
	lucknum1 := rand.Intn(z)
	for i := 1; i <= x; i++ {
		for j := 1; j <= y; j++ {
			lucknum2 := rand.Intn(z)
			if lucknum2 == lucknum1 {
				fmt.Printf("当前是你第%d次十连抽奖，恭喜您，中奖了！\n", i)
				return
			}
		}
		fmt.Printf("当前是你第%d次十连抽奖，抽奖结果：未中奖\n", i)
	}

}

func main() {
	rand.Seed(time.Now().Unix())
	var (
		a1 int = rand.Intn(10)
		b2 int = rand.Intn(10)
		c3 int
	)
	fmt.Println("请回答下面的问题")
	fmt.Printf("%d + %d = ", a1, b2)
	fmt.Scanln(&c3)
	if a1+b2 == c3 {
		var num int = rand.Intn(10) + 1
		fmt.Printf("恭喜你 回答正确，获取%d次十连抽奖机会\n", num)
		luckDraw(num, 10, 10000)
	}
}

// 未实现猜数字的逻辑
