package main

import (
	"fmt"
	"math/rand"
	"time"
)

// 做一个抽卡程序, 中奖率万分之一, 看看你多少个10连能中奖
// 请回答如下问题, 比如: a + b =:  m
// 恭喜你 回答正确，随机获取n[1~10]的抽奖机会
// 当前是你第1次抽奖: 抽奖结果 未中奖
// 当前是你第2次抽奖. 抽奖结果 未中奖
// 当前是你第3次抽奖. 抽奖结果 未中奖
//解题思路：
// 1、流程：答疑--随机获取N次抽奖机会--抽奖
// 2、实现：计算机出一个算数题控制台回答--random出抽奖次数--种子10000出随机数让用户抽
func main() {
	var input int
	a, b := 5, 6
	fmt.Println("请问5+6等于多少？")
	fmt.Scan(&input)
	if input == a+b {
		fmt.Println("恭喜你答对了！您获得了10次抽奖机会!")
		fmt.Println("本期中奖号码为: 【9527】")

		rand.Seed(time.Now().Unix())
		for i := 1; i <= 10; i++ {
			num := rand.Intn(10000) + 1
			if num == 68 {
				fmt.Printf("当前是你第%d次抽奖. 抽奖结果:%d ,恭喜你中奖了！\n", i, num)
				break
			}
			fmt.Printf("当前是你第%d次抽奖. 抽奖结果:%d ,未中奖\n", i, num)
		}

	} else {
		fmt.Println("输入错误！")
	}
}
