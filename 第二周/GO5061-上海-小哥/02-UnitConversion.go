package main

import (
	"fmt"
)

// HumanBytesLoaded 单位转换(B KB MB GB TB EB)
func HumanBytesLoaded(bytesLength int64) string {
	var resp string
	var  (
		B int64 = 1
		KB int64 = B * 1024
		MB int64 = KB * 1024
		GB int64 = MB * 1024
		TB int64 = GB * 1024
		PB int64 = TB * 1024
		EB int64 = PB * 1024
	)
	// bytesLength
	fmt.Printf("需要转换的值: %dB\n", bytesLength)
	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
	switch {
	case bytesLength < KB && bytesLength >= B:
		resp =  fmt.Sprintf("%dB",bytesLength)
	case bytesLength < MB && bytesLength >= KB:
		resp = fmt.Sprintf("%dKB",bytesLength/KB)
	case bytesLength < GB && bytesLength >= MB:
		resp = fmt.Sprintf("%dMB",bytesLength/MB)
	case bytesLength < TB && bytesLength >= GB:
		resp = fmt.Sprintf("%dGB",bytesLength/GB)
	case bytesLength < PB && bytesLength >= TB:
		resp = fmt.Sprintf("%dTB",bytesLength/TB)
	case bytesLength < EB && bytesLength >= PB:
		resp = fmt.Sprintf("%dPB",bytesLength/PB)
	case bytesLength >= EB:
		resp = fmt.Sprintf("%dEB",bytesLength/EB)
	default:
		fmt.Println("输入的数字不合法")
	}
	// 返回转换的结果
	return resp
}
func main() {
	ret := HumanBytesLoaded(111111111)
	fmt.Printf("转换结果为: %s",ret)
}
