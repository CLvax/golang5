package main

import (
	"fmt"
	"strconv"
)

func main() {
	// 定义int类型数组
	var a = [...]string{
		"1000100001100011",
		"0110011100001101",
		"0101010110011100",
		"0110101100100010",
		"0111101001111111",
		"0100111000101101",
		"0101011011111101",
		"0111111010100010",
	}
	// 循环数组
	for _, v := range a {
		// 类型转化
		num, err := strconv.ParseInt(v, 2, 64)
		// fmt.Println(num,err)
		if err == nil {
			fmt.Printf("二进制: %s, unicode: %U,字符: %c\n", v, num, num)
		}

	}

}