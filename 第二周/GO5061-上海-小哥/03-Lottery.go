package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	var a int
	fmt.Print("4 + 5 = ")
	fmt.Scanf("%d", &a)
	if a == 9 {
		fmt.Println("回答正确")
		b := 1000  // 定义中奖号码
		rand.Seed(time.Now().UnixNano())
		num := rand.Intn(9) + 1
		fmt.Printf("恭喜获得抽奖次数: %d\n", num)
		// 开始抽奖
		for i := 1; i <= num; {
			fmt.Printf("当前为第%d次抽奖\n", i)
			num1 := rand.Intn(10000)
			fmt.Printf("当前抽中数: %d\n", num1)
			if num1 == b {
				fmt.Println("恭喜您中奖")
				break
			}
			i = i + 1
		}

	}
}
