package main

import (
	"fmt"
	"strconv"
)

func HumanBytesConvert(bytesLength float64) (Hsize string) {
	const (
		B  = iota
		KB = 1 << (10 * iota)
		MB
		GB
		TB
		PB
		EB
	)
	units := map[string]float64{
		"B": B, "KB": KB, "MB": MB, "GB": GB, "TB": TB, "PB": PB, "EB": EB,
	}

	for UName, Num := range units {
		quotient := bytesLength / Num
		if 1 <= quotient && quotient < 1024 {
			UConvert := strconv.FormatFloat(quotient, 'f', 3, 64)
			Hsize = UConvert + UName
			break
		}
	}
	return Hsize

}
func main() {
	// 参考G5044-上海-徐，GO5005-北京-崔凯鹏
	var bytesize float64
	fmt.Printf("请输入字节大小，例如102400： ")
	fmt.Scan(&bytesize)
	HumanSize := HumanBytesConvert(bytesize)
	fmt.Println(HumanSize)
}
