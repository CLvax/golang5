package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	//用户输入结果
	key := 0
	num := 0
	//生成随机1～10000的一个幸运数字
	rand.Seed(time.Now().UnixNano())
	luckyNum := rand.Intn(100)
	fmt.Printf("本轮幸运数字为：%d\n", luckyNum)
	//循环判断结果
	for {
		m := rand.Intn(10)
		n := rand.Intn(10)
		fmt.Println("请回答以下问题：")
		fmt.Printf("%d + %d  = ", m, n)
		fmt.Scanln(&key)
		if key != m+n {
			fmt.Println("回答错误,请再次作答")
		} else {
			chanceNum := rand.Intn(10) + 1
			fmt.Printf("恭喜你获得%d次机会！！！\n", chanceNum)
			for i := 1; i <= chanceNum; i++ {

				fmt.Printf("当前是你第%d次抽奖, 请输入数字(0-10000): ", i)
				fmt.Scanln(&num)
				switch num {
				case luckyNum:
					fmt.Println("恭喜你,中奖！！！")
					return
				default:
					fmt.Println("抽奖结果 未中奖")
				}
			}
		}
		fmt.Println("让我们再来一轮吧!!!")
	}

}

// 可以考虑进行一下提示
