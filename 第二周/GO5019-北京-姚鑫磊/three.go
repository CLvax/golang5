package main

import (
	"fmt"
	"math/rand"
	"time"
)

func Day2(a, b, c int) {
	A := rand.Intn(c)
	for i := 1; i <= a; i++ {
		for j := 1; j <= b; j++ {
			B := rand.Intn(c)
			if B == A {
				fmt.Printf("当前是第%d次十连抽，恭喜你，中奖了！\n", i)
				return
			}
		}
		fmt.Printf("当前是第%d次十连抽，抽奖结果为：未中奖！\n", i)
	}
}
func main() {
	rand.Seed(time.Now().Unix())
	var (
		a1 int = rand.Intn(10)
		b1 int = rand.Intn(10)
		c1 int
	)
	fmt.Println("请回答问题：")
	fmt.Printf("%d + %d =", a1, b1)
	fmt.Scanln(&c1)
	if a1+b1 == c1 {
		var C int = rand.Intn(5) + 1
		fmt.Printf("恭喜你 回答正确，获取%d次十连抽的机会\n", C)
		Day2(C, 10, 10000)
	}
}

//未完成对抽奖的判断
