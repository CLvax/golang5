package main

import (
	"fmt"
)

func main() {
	fmt.Println(HumanBytesLoaded(1024 * 106724 * 202312423))
}

func HumanBytesLoaded(bytesLength int64) string {
	var resp string

	num := float64(bytesLength)
	//fmt.Printf("需要转换的值: %dB", bytesLength)

	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB

	// 返回转换的结果
	const (
		B  float64 = 1
		KB float64 = B * 1024
		MB float64 = KB * 1024
		GB float64 = MB * 1024
		TB float64 = GB * 1024
		EB float64 = TB * 1024
	)

	if num/EB >= 1.0 {
		resp = fmt.Sprintf("%.3fEB", num/EB)
	} else if num/EB < 1.0 && num/TB >= 1.0 {
		resp = fmt.Sprintf("%.3fTB", num/TB)
	} else if num/TB < 1.0 && num/GB >= 1 {
		resp = fmt.Sprintf("%.3fGB", num/GB)
	} else if num/GB < 1 && num/MB >= 1 {
		resp = fmt.Sprintf("%.3fMB", num/MB)
	} else if num/MB < 1 && num/KB >= 1 {
		resp = fmt.Sprintf("%.3fKB", num/KB)
	} else {
		resp = fmt.Sprintf("%.3fB", num/B)
	}
	return resp

}

// 参数由用户输入试下
