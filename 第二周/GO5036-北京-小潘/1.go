package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {

	b := []string{"1000 1000 0110 0011",
		"0110 0111 0000 1101",
		"0101 0101 1001 1100",
		"0110 1011 0010 0010",
		"0111 1010 0111 1111",
		"0100 1110 0010 1101",
		"0101 0110 1111 1101",
		"0111 1110 1010 0010"}

	for _, v := range b {
		v = strings.Replace(v, " ", "", -1)
		a, _ := strconv.ParseInt(v, 2, 64)
		fmt.Printf("%c", a)
	}
	fmt.Println()
}

// 完成的不错，换行用Printf实现试下