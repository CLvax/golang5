package main

/**
 * @Classname main
 * @Description TODO
 * @author cjf
 * @Date 2021/7/15 13:51
 * @Version V1.0
 */

import (
	"fmt"
	"github.com/pizicaiman/cmtools"
)

func main() {

	hostname := cmtools.GetHostName()
	ip := cmtools.GetLocalIp()
	ts := cmtools.GetNowTimeStr()
	fmt.Println(hostname, ip, ts)

}
