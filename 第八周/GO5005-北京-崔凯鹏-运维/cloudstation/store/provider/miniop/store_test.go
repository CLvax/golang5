package miniop_test

import (
	"cloudstation/store/provider/miniop"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	endpoint = "192.168.102.246:9000"
	ak       = "AKIAIOSFODNN7EXAMPLE"
	sk       = "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
	useSSL   = false
)

var (
	// 创建一个叫mymusic的存储桶。
	bucketName = "mymusic"
	location   = "us-east-1"
)

var (
	// 上传一个zip文件。
	objectKey     = "store.go1"
	localFilePath = "store.go"
)

func TestUploadFile(t *testing.T) {
	should := assert.New(t)
	uploader, err := miniop.NewUploader(endpoint, ak, sk)
	if assert.NoError(t, err) {
		err = uploader.UploadFile(bucketName, objectKey, localFilePath)
		should.NoError(err)
	}
}

// func TestUploadFile1(t *testing.T) {
// 	should := assert.New(t)

// 	uploader, err := aliyun.NewUploader("", "", "")
// 	if should.NoError(err) {
// 		err = uploader.UploadFile(bucketName, objectKey, localFilePath)
// 		should.NoError(err)
// 	}
// }
