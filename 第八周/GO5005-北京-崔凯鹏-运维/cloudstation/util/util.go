package util

import (
	// "cloudstation/store"
	"fmt"

	"github.com/AlecAivazis/survey/v2"
	"github.com/go-playground/validator"
)

const (
	bu = 1 << 10
	kb = 1 << 20
	mb = 1 << 30
	gb = 1 << 40
	tb = 1 << 50
	eb = 1 << 60
)

func Validate(user interface{}) error {
	validate := validator.New()
	return validate.Struct(user)

}

// HumanBytesLoaded 单位转换
func HumanBytesLoaded(bytesLength int64) string {
	if bytesLength < bu {
		return fmt.Sprintf("%dB", bytesLength)
	} else if bytesLength < kb {
		return fmt.Sprintf("%.2fKB", float64(bytesLength)/float64(bu))
	} else if bytesLength < mb {
		return fmt.Sprintf("%.2fMB", float64(bytesLength)/float64(kb))
	} else if bytesLength < gb {
		return fmt.Sprintf("%.2fGB", float64(bytesLength)/float64(mb))
	} else if bytesLength < tb {
		return fmt.Sprintf("%.2fTB", float64(bytesLength)/float64(gb))
	} else {
		return fmt.Sprintf("%.2fEB", float64(bytesLength)/float64(tb))
	}
}

func GetAccessKeyFromInputV2(AccessKey *string) {
	prompt := &survey.Password{
		Message: "请输入access key: ",
	}
	survey.AskOne(prompt, AccessKey)
}

func InputUserPassword(username,password *string) {
	fmt.Print("请输入账号：")
	fmt.Scan(username)
	prompt := &survey.Password{
		Message: "请输入用户密码: ",
	}
	survey.AskOne(prompt, password)
}