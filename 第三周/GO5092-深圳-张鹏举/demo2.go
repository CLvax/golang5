package main

import "fmt"

func main() {

	//  数学   语文   英语
	//   88     88    90
	//   66
	//   ...
	//   avg
	var scores = [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}
	new_scores := []float32{}

	for i := 0; i < len(scores[0]); i++ {

		sum := 0
		for j := 0; j < len(scores); j++ {
			sum += scores[j][i]
		}

		scores_avg := float32(sum) / float32(len(scores))

		//fmt.Println(scores_avg)

		new_scores = append(new_scores, scores_avg)

	}
	fmt.Println(new_scores)
	fmt.Printf("数学平均成绩是: %.2f\n", new_scores[0])
	fmt.Printf("语文平均成绩是: %.2f\n", new_scores[1])
	fmt.Printf("英语平均成绩是: %.2f\n", new_scores[2])
}

