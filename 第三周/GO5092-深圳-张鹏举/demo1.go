package main

import (
	"fmt"
	"unsafe"
)

/*
通过内存地址访问Tag的值
*/

type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

func main() {
	x := new(Book)
	x.Tag = []string{"map", "slice", "func"}

	tag := (*[]string)(unsafe.Pointer(uintptr(unsafe.Pointer(x)) + unsafe.Offsetof(x.Tag)))

	fmt.Printf("结构体Book字段Tag的内存地址为: %p\n", tag)

	for index, value := range *tag {
		fmt.Printf("Tag[%d]的值为%s\n", index, value)
	}

}

