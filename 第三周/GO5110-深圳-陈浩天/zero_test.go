package zero

// 本程序为测试程序，执行go test


import (
	"fmt"
	"testing"
	"unsafe"
)

//通过内存地址访问Tag的值（须连续内存地址）
//1.将地址转换成pointer类型地址
//2.将pointer类型地址转换成uintptr类型地址
//3.uintptr += unsafe.Sizeof(当前自己) 结果则等于下个元素的起始内存地址
//4.将uintptr类型指针转换成Pointer类型地址
//5.再将pointer类型地址转换成普通数据类型地址就可以通过*name 访问内容了
type person struct {
	Name string
	Age int
	Genders string
	Hobby []string
}

func TestZero(t *testing.T) {
	p := person{
		Name: "zhangsa",
		Age: 18,
		Genders: "唱歌 篮球 喝酒",
		Hobby: make([]string,3),
	}

	p.Hobby[0] = "a"
	p.Hobby[1] = "b"
	p.Hobby[2] = "c"

	Zero(p)
	UnsafeShow(p)
}

//通过内存地址访问Tag的值（须连续内存地址）
func Zero(p person) {
	// 这里&p.Name 拿到的是他的起始地址
	resPointer := unsafe.Pointer(&p.Name)

	// 将pointer地址转换成uintptr
	resPointerUintptr := uintptr(resPointer)

	// 起始地址加上自身所占内存长度（unsafe.Sizeof(p.Name)）则等于下个元素的内存地址
	resPointerUintptr += 24

	// 将Uintptr类型转换会Pointer
	resPointerUintptrPointer :=  unsafe.Pointer(resPointerUintptr)


	// Pointer类型地址 转换成普通类型内存地址
	demo := (*string)(resPointerUintptrPointer)
	fmt.Printf("下一个值为: %v",*demo)
}


func UnsafeShow(p person) {
	fmt.Printf("p.Name 占用内存大小（单位：字节）%v\t内存地址 %v\n" ,unsafe.Sizeof(p.Name),&p.Name)
	fmt.Printf("p.Age 占用内存大小（单位：字节）%v\t内存地址 %v\n" ,unsafe.Sizeof(p.Age),&p.Age)
	fmt.Printf("p.Age 占用内存大小（单位：字节）%v\t内存地址 %v\n" ,unsafe.Sizeof(p.Genders),&p.Genders)
	fmt.Printf("p.Name 占用内存对齐（单位：字节）%v\n" ,unsafe.Alignof(p.Name))
	fmt.Printf("p.Age 占用内存对齐（单位：字节）%v\n" ,unsafe.Alignof(p.Age))
	fmt.Printf("p.Age 占用内存对齐（单位：字节）%v\n" ,unsafe.Alignof(p.Genders))
	fmt.Printf("p.Name 占用内存偏移量（单位：字节）%v\n" ,unsafe.Offsetof(p.Name))
	fmt.Printf("p.Age 占用内存偏移量（单位：字节）%v\n" ,unsafe.Offsetof(p.Age))
	fmt.Printf("p.Age 占用内存偏移量（单位：字节）%v\n" ,unsafe.Offsetof(p.Genders))
}

