package main

import (
	"fmt"
)
type Student struct {
	Name 		string
	Number		int
	Subjects	[]string
	Scores		[]float32
}
type Class struct {
	Name		string
	Number		int
	Students	[]Student
}
func Class_avg(c Class) {
	//控制Scores的索引，进行循环
	for i := 0; i < len(c.Students[0].Scores); i++ {
		var sum float32 = float32(0)
		//控制students的索引，进行循环
		for j := 0; j < len(c.Students); j++ {
			sum += c.Students[j].Scores[i]
		}
		scores_avg := sum/float32(len(c.Students))
		fmt.Printf("%s的%s的平均分是%v\n",c.Name,c.Students[0].Subjects[i],scores_avg)
	}
}
func main() {
	class1 := Class{
		Name: "一年级",
		Number: 01,
		Students: []Student{
			{
				Name: "风清扬",
				Number: 0101,
				Subjects: []string{"数学","语文","英语"},
				Scores: []float32{100.0,92.0,97.0},
			},
			{
				Name: "令狐冲",
				Number: 0102,
				Subjects: []string{"数学","语文","英语"},
				Scores: []float32{98.0,90.0,94.0},
			},
		},
	}
	class2 := Class{
		Name: "二年级",
		Number: 02,
		Students: []Student{
			{
				Name: "东方不败",
				Number: 0201,
				Subjects: []string{"数学","语文","英语"},
				Scores: []float32{100.0,95.0,99.0},
			},
			{
				Name: "岳不群",
				Number: 0202,
				Subjects: []string{"数学","语文","英语"},
				Scores: []float32{95.0,92.0,91.0},
			},
		},
	}
	Class_avg(class1)
	Class_avg(class2)
}
