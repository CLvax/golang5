package main

import (
	"fmt"
)

func main() {
	scores := [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}
	var avg []float32
	//控制切片的第二个索引
    for i := 0; i < len(scores[0]); i++ {
		sum := 0
		//控制切片的第一个索引
		for j := 0; j < len(scores); j++ {
			sum += scores[j][i]
		}
		scores_avg := float32(sum)/float32(len(scores))
		avg = append(avg,scores_avg)
	}
	fmt.Printf("数学成绩平均分是:%v\n",avg[0])
	fmt.Printf("语文成绩平均分是:%v\n",avg[1])
	fmt.Printf("英语成绩平均分是:%v\n",avg[2])
}
