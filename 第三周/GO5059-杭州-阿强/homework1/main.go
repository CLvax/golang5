package main

import (
	"fmt"
	"unsafe"
)

type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

const (
	// 取到结构体成员偏移量
	N = unsafe.Offsetof(Book{}.Tag)
)

func main() {
	b := &Book{Tag: []string{"abc", "def", "hjk"}}
	tagaddr := (*[]string)(unsafe.Pointer(uintptr(unsafe.Pointer(b)) + N))
	fmt.Println(*tagaddr)
}

// 内存地址。可以考虑格式化输出