package main

import (
	"fmt"
)

func main() {
	scores := [][]int{
		// 数学，语文，英语
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}
	totalMath := 0
	totalChina := 0
	totalEng := 0
	for _, score := range scores {
		totalMath += score[0]
		totalChina += score[1]
		totalEng += score[2]
	}
	fmt.Printf("%d,%d,%d\n", totalMath/len(scores), totalChina/len(scores), totalEng/len(scores))
}

// 可以考虑使用精度