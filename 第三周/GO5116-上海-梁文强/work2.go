package main

import "fmt"

//  数学   语文   英语
//   88     88    90
//   66
//   ...
//   avg

var scores = [][]int{
	{88, 88, 90},
	{66, 99, 94},
	{75, 84, 98},
	{93, 77, 66},
}

func getavgscore() {
	var math_sum, chn_sum, eng_sum int
	student_count := len(scores)
	for _, line := range scores {
		math_sum += line[0]
		chn_sum += line[1]
		eng_sum += line[2]
	}
	fmt.Printf("Avg of math,chn,eng: %d,%d,%d\n", math_sum/student_count, chn_sum/student_count, eng_sum/student_count)
}

func main() {
	getavgscore()
}
