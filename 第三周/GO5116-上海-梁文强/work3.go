package main

import "fmt"

type Student struct {
	Name    string
	Number  int
	Subject []string
	Scores  []int
}

type Class struct {
	Name     string
	Number   int
	Students []Student
}

var cl1 = Class{
	Name:   "grade1",
	Number: 1,
	Students: []Student{
		{
			Name:    "zhangsan",
			Number:  101,
			Subject: []string{"chn", "math", "eng"},
			Scores:  []int{99, 98, 95},
		},
		{
			Name:    "lisi",
			Number:  102,
			Subject: []string{"chn", "math", "eng"},
			Scores:  []int{100, 99, 99},
		},
		{
			Name:    "wangwu",
			Number:  103,
			Subject: []string{"chn", "math", "eng"},
			Scores:  []int{87, 89, 93},
		},
	},
}

var cl2 = Class{
	Name:   "grade1",
	Number: 2,
	Students: []Student{
		{
			Name:    "zhaoliu",
			Number:  104,
			Subject: []string{"chn", "math", "eng"},
			Scores:  []int{98, 99, 95},
		},
		{
			Name:    "songbao",
			Number:  105,
			Subject: []string{"chn", "math", "eng"},
			Scores:  []int{96, 94, 83},
		},
	},
}

func GetAvgScore(cl *Class) {
	Name := cl.Name
	Number := cl.Number
	var chn_sum, math_sum, eng_sum int
	students_count := len(cl.Students)
	for _, line := range cl.Students {
		chn_sum += line.Scores[0]
		math_sum += line.Scores[1]
		eng_sum += line.Scores[2]
	}
	chn_avg := chn_sum / students_count
	math_avg := math_sum / students_count
	eng_avg := eng_sum / students_count
	fmt.Printf("Avg of %v %d %s : %d\n", Name, Number, "chn", chn_avg)
	fmt.Printf("Avg of %v %d %s : %d\n", Name, Number, "math", math_avg)
	fmt.Printf("Avg of %v %d %s : %d\n", Name, Number, "eng", eng_avg)
}

func main() {
	GetAvgScore(&cl1)
	GetAvgScore(&cl2)

}
