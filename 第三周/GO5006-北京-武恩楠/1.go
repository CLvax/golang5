package main

import (
	"fmt"
	"unsafe"
)

//通过内存地址访问Tag的值
type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

func main() {
	b := Book{
		Tag: []string{"abc", "def", "hjk"},
	}

	p1 := unsafe.Pointer(&b.Tag[0])
	p2 := unsafe.Pointer(&b.Tag[1])
	p3 := unsafe.Pointer(&b.Tag[2])
	ty1 :=(*string)(unsafe.Pointer(uintptr(p1)))
	ty2 :=(*string)(unsafe.Pointer(uintptr(p2)))
	ty3 :=(*string)(unsafe.Pointer(uintptr(p3)))
	fmt.Println(*ty1,*ty2,*ty3)

}

// 