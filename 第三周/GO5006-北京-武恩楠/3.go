package main

import "fmt"

// 使用结构体表示班级和学生，请计算每个班级学科平均分
// Student 名称(Name) 学号(Number) 科目(Subjects) 成绩(Scores)
// Class   名称(Name) 编号(Number) 学员(Students)
// Class   实现一个平均值的方法

type Student struct {
	Name string
	Number int
	Subjects []string
	Scores []int
}

type Class struct {
	Name string
	Number int
	Students []Student
}

func (c Class)ClassAvg()  {

	// 根据列数循环
	for i:=0;i<len(c.Students[0].Scores);i++{
		var Sum float64
		for j:=0;j<len(c.Students);j++{
			sc:=c.Students[j].Scores[i]
			//fmt.Println(sc)

			Sum += float64(sc)
		}

		fmt.Printf("学科%d 的平均分为%v\n",i,Sum/float64(len(c.Students)))
	}
}

func main() {
	//fmt.Println(s1)
	c1 :=&Class{
		Name: "1班",
		Number: 2,
		Students: []Student{
			Student{
				Name: "LiHong",
				Number: 1,
				Subjects: []string{"Chinese","Math","English"},
				Scores: []int{90,43,78},
			},
			Student{
				Name: "ZhangSan",
				Number: 2,
				Subjects: []string{"Chinese","Math","English"},
				Scores: []int{79,72,23},
			},
		},
	}


	c2 :=&Class{
		Name: "2班",
		Number: 2,
		Students: []Student{
			Student{
			Name: "LiMing",
			Number: 1,
			Subjects: []string{"Chinese","Math","English"},
			Scores: []int{90,78,78},
		},
		Student{
			Name: "LiSi",
			Number: 2,
			Subjects: []string{"Chinese","Math","English"},
			Scores: []int{79,72,93},
		},
		},
	}

	c1.ClassAvg()
	c2.ClassAvg()
}

// 写得不错