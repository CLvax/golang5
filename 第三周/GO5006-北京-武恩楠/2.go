package main

import "fmt"

//  数学   语文   英语
//   88     88    90
//   66
//   ...
//   avg

func Avg(scores [][]int){
	// 取列数
	for i:=0;i<len(scores[0]);i++{
		var Sum float64
		for j:=0;j<len(scores);j++{
			Sum += float64(scores[j][i])
		}
		fmt.Println(Sum/float64(len(scores)))
	}
}

func main() {
	scores := [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}
	Avg(scores)
	//var Mathsum,Chinesum,Englishsum int
	//for i:=0;i<4;i++{
	//	for j:=0;j<3;j++{
	//		// 打印每个人的成绩
	//		fmt.Printf("%d ",scores[i][j])
	//		if j==0{
	//			// 获取scores[0][0] [1][0] [2][0] [3][0]的值，并求和
	//			Mathsum += scores[i][j]
	//		}
	//		if j==1{
	//			// 取scores[0][1] [1][1] [2][1] [3][1]的值，并求和
	//			Chinesum += scores[i][j]
	//		}
	//		if j==2{
	//			// 取scores[0][2] [1][2] [2][2] [3][2]的值，并求和
	//			Englishsum += scores[i][j]
	//		}
	//	}
	//	fmt.Println()
	//}
	//fmt.Println("---avg---")
	//// 求各科平均数
	//fmt.Printf("%d %d %d",Mathsum/4,Chinesum/4,Englishsum/4)
}

// 记得换行
