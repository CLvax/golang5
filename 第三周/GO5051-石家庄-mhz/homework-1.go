package main

import (
	"fmt"
	"unsafe"
)

//题目：
//通过内存地址方位Tag的值
// type Book struct {
// 	Title  string
// 	Author string
// 	Page   uint
// 	Tag    []string
// }
// b := &Book{Tag: []string{"abc", "def", "hjk"}}
// 根据结构体的内存地址, 计算出Tag的内存地址, 并访问

type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

func main() {
	b := &Book{Tag: []string{"abc", "def", "hjk"}}
	fmt.Println(unsafe.Offsetof(b.Tag))
	pTag := (*[]string)(unsafe.Pointer(uintptr(unsafe.Pointer(b)) + unsafe.Offsetof(b.Tag)))
	fmt.Printf("Tag 的内存地址：%p\n", pTag)
	fmt.Printf("Tag 的内容是：%v\n", *pTag)

	//扩展一下：利用unsafe.Pointer()求出Tag[1]的地址和内容
	fmt.Printf("Tag 的指针内容：%p\n", b.Tag)
	fmt.Printf("Tag[0] 的内存地址：%p\n", &b.Tag[0])
	pTag1 := (*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&b.Tag[0])) + unsafe.Sizeof(b.Tag[0])))
	fmt.Printf("Tag[1] 的内存地址：%p\n", pTag1)
	fmt.Printf("Tag[1] 的内容是：%v\n", *pTag1)

}

// Tag 的内存地址：0xc000042068
// Tag 的内容是：[abc def hjk]
// Tag 的指针内容：0xc0000703c0
// Tag[0] 的内存地址：0xc0000703c0
// Tag[1] 的内存地址：0xc0000703d0
// Tag[1] 的内容是：def

// 居然想到了格式化输出下
