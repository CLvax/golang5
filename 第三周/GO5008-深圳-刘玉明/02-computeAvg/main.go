package main

import (
	"fmt"
)

var scores = [][]int{
	{88, 88, 90},
	{66, 99, 94},
	{75, 84, 98},
	{93, 77, 66},
}

func main() {
	scores := [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}
	// fmt.Println(len(scores))
	studentavg := []float64{}
	var subjectAvg float64
	for i := 0; i <= len(scores[0])-1; i++ {
		subjectAvg = 0
		for j := 0; j <= len(scores)-1; j++ {
			subjectAvg = subjectAvg + float64(scores[j][i])
			// fmt.Println(scores[j][i])
		}
		studentavg = append(studentavg, subjectAvg/float64(len(scores)))
		fmt.Printf("学课%d平均分：%.2f\n", i+1, studentavg[i])
	}
}

// 不错，可以想到保留精度问题
