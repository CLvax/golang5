package avetageStruct

import (
	"fmt"
	"testing"
)

type Student struct {
	Name     string
	Number   int
	Subjects map[string]float64
	Scores   int
}

func NewStudent(name string, number int, math, language, english int) *Student {
	return &Student{
		Name:   name,
		Number: number,
		Subjects: map[string]float64{
			"math":     float64(math),
			"language": float64(language),
			"english":  float64(english),
		},
		Scores: math + language + english,
	}
}

func (s *Student) GetMath() float64 {
	return s.Subjects["math"]
}

func (s *Student) GetLanguage() float64 {
	return s.Subjects["language"]
}

func (s *Student) GetEnglish() float64 {
	return s.Subjects["english"]
}

func (s *Student) SetMath(scores int) {
	s.Subjects["math"] = float64(scores)
	s.TotalScore()
}

func (s *Student) SetLanguage(scores int) {
	s.Subjects["language"] = float64(scores)
	s.TotalScore()
}

func (s *Student) SetEnglish(scores int) {
	s.Subjects["english"] = float64(scores)
	s.TotalScore()
}

func (s *Student) TotalScore() {
	for _, v := range s.Subjects {
		s.Scores += int(v)
	}
}

type Class struct {
	Name     string
	Number   string
	Students []*Student
}

func NewClass(name string, number string, students []*Student) *Class {
	return &Class{
		Name:     name,
		Number:   number,
		Students: students,
	}
}

func (c *Class) SubjectAverage() map[string]float64 {
	var (
		mathScores        float64
		languageScores    float64
		englishScores     float64
		SubjectAverageMap = make(map[string]float64, 3)
	)
	for _, s := range c.Students {
		mathScores += s.GetMath()
		languageScores += s.GetLanguage()
		englishScores += s.GetEnglish()
	}
	SubjectAverageMap["mathAverageScore"] = mathScores / float64(len(c.Students))
	SubjectAverageMap["languageAverageScore"] = languageScores / float64(len(c.Students))
	SubjectAverageMap["englishAverageScore"] = englishScores / float64(len(c.Students))

	for k, v := range SubjectAverageMap {
		fmt.Printf("%-20s %-5.2f \n", k, v)
	}
	return SubjectAverageMap
}

func TestAverageStruct(t *testing.T) {
	s := []*Student{
		NewStudent("zhao", 1, 88, 88, 90),
		NewStudent("qian", 2, 66, 99, 94),
		NewStudent("sun", 3, 75, 84, 98),
		NewStudent("li", 4, 93, 77, 66),
	}
	class := NewClass("Class1", "1", s)
	class.SubjectAverage()

}
