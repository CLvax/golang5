package main

import "fmt"

//使用结构体表示班级和学生，请计算每个班级学科平均分

//定义学生结构体
type Student struct {
	Name     string
	Number   int
	Subjects []string
	scores   []int
}

//定义班级结构体
type Class struct {
	Name     string
	Number   int
	Students []Student
}

//计算学生总数
func sumStu(class Class) int {
	sum := len(class.Students)
	return sum
}

//提取每个学生的单科分数
func subScore(stu Student, sub string) int {
	score := 0
	switch sub {
	case "数学":
		score = stu.scores[0]
	case "语文":
		score = stu.scores[1]
	case "英语":
		score = stu.scores[2]
	default:
		score = 0
	}
	return score
}

//计算所有学生单科总分

func sumScores(class Class, sub string) int {
	sum := 0
	for _, v := range class.Students {
		//v为stu结构体
		//调用subScore函数，获取当前stu sbu学科的分数
		score := subScore(v, sub)
		//所有学生score相加获取总分
		sum += score
	}
	return sum
}

//计算单科平均分
//单科总分/学生人数
func Avg(class Class, sub string) float32 {
	avg := float32(sumScores(class, sub)) / float32(sumStu(class))
	return avg
}

func main() {
	stu1 := Student{
		Name:     "stu1",
		Number:   1001,
		Subjects: []string{"数学", "语文", "英语"},
		scores:   []int{88, 88, 90},
	}
	stu2 := Student{
		Name:     "stu2",
		Number:   1002,
		Subjects: []string{"数学", "语文", "英语"},
		scores:   []int{88, 88, 90},
	}
	stu3 := Student{
		Name:     "stu3",
		Number:   1003,
		Subjects: []string{"数学", "语文", "英语"},
		scores:   []int{88, 88, 90},
	}
	stu4 := Student{
		Name:     "stu4",
		Number:   2001,
		Subjects: []string{"数学", "语文", "英语"},
		scores:   []int{1, 2, 3},
	}
	stu5 := Student{
		Name:     "stu5",
		Number:   2002,
		Subjects: []string{"数学", "语文", "英语"},
		scores:   []int{2, 3, 4},
	}

	classGolang := Class{
		Name:     "Golang",
		Number:   1,
		Students: []Student{stu1, stu2, stu3},
	}

	classPython := Class{
		Name:     "Python",
		Number:   2,
		Students: []Student{stu4, stu5},
	}

	fmt.Printf("Golang班数学学科平均分为:%.2f\n", Avg(classGolang, "数学"))
	fmt.Printf("Golang班语文学科平均分为:%.2f\n", Avg(classGolang, "语文"))
	fmt.Printf("Golang班英语学科平均分为:%.2f\n", Avg(classGolang, "英语"))

	fmt.Printf("Python班数学学科平均分为:%.2f\n", Avg(classPython, "数学"))
	fmt.Printf("Python班语文学科平均分为:%.2f\n", Avg(classPython, "语文"))
	fmt.Printf("Python班英语学科平均分为:%.2f\n", Avg(classPython, "英语"))
}


// 可以使用结构体class来定义班级