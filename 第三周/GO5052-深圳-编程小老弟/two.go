package main

import "fmt"

//使用二维切片表示一组学生的各科成绩，计算所有学生的平均分

func main() {
	//  数学   语文   英语
	//   88     88    90
	//   66
	//   ...
	//   avg
	scores := [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}
	//求学生总人数
	numberofstu := len(scores)
	fmt.Println("学生总人数:", numberofstu)

	//科目数
	numberofsub := len(scores[0])
	fmt.Println("每个学生的课程数:", numberofsub)

	//学生总分数
	sum := 0
	for k1, _ := range scores {
		for _, v := range scores[k1] {
			sum += v
		}
	}
	fmt.Println("学生总分数", sum)

	//所有学生的平均分
	avg := float32(sum) / float32(numberofstu*numberofsub)
	fmt.Printf("所有学生的单科平均分:%.2f \n", avg)

}

// 有可能布置的作业有歧义