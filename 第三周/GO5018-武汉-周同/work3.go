package main

import "fmt"

type Student struct {
	Name     string
	Number   int
	Subjects []string
	Scores   []float64
}

type Class struct {
	Name     string
	Number   int
	Students []Student
}

func (c *Class) ClassAvg() {
	//语文总分
	ywSum := 0.0
	//数学总分
	sxSum := 0.0
	//英语总分
	yySum := 0.0

	for i := 0; i < len(c.Students); i++ {
		ywSum += c.Students[i].Scores[0]
		sxSum += c.Students[i].Scores[1]
		yySum += c.Students[i].Scores[2]

	}
	fmt.Printf("%v%v班%v学科平均分是：%v\n", c.Name, c.Number, c.Students[0].Subjects[0], fmt.Sprintf("%.2f", ywSum/float64(len(c.Students))))
	fmt.Printf("%v%v班%v学科平均分是：%v\n", c.Name, c.Number, c.Students[0].Subjects[1], fmt.Sprintf("%.2f", sxSum/float64(len(c.Students))))
	fmt.Printf("%v%v班%v学科平均分是：%v\n", c.Name, c.Number, c.Students[0].Subjects[2], fmt.Sprintf("%.2f", yySum/float64(len(c.Students))))

}

func main() {
	var class = &Class{
		Name:   "一年级",
		Number: 11,
		Students: []Student{
			{
				Name:     "张三",
				Number:   11001,
				Subjects: []string{"语文", "数学", "英语"}, //
				Scores:   []float64{81, 90, 50},
			},
			{
				Name:     "李四",
				Number:   11002,
				Subjects: []string{"语文", "数学", "英语"},
				Scores:   []float64{70, 80, 60},
			},
			{
				Name:     "王五",
				Number:   11002,
				Subjects: []string{"语文", "数学", "英语"},
				Scores:   []float64{60, 40, 30},
			},
		},
	}

	class.ClassAvg()
}

// 尝试下不使用嵌套定义
