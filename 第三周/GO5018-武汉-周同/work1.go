package main

import (
	"fmt"
	"unsafe"
)

type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

func main() {
	book1 := Book{
		Title:  "english",
		Author: "",
		Page:   64,
		Tag:    []string{"a", "b", "c"},
	}
	//用指针运算打印出book1.Tag[1]的值 b
	fmt.Println(*(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&book1.Tag[0])) + unsafe.Sizeof(book1.Tag[0]))))

	// arr := [5]int{10, 20, 30}
	// fmt.Println(*(*int)(unsafe.Pointer(uintptr(unsafe.Pointer(&arr)) + unsafe.Sizeof(arr[0]))))
	// sli := []string{"i", "love", "golang"}
	// fmt.Println(&sli[0])
	// fmt.Println(*(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&sli[0])) + unsafe.Sizeof(sli[0]))))
}

// 还有内存地址
