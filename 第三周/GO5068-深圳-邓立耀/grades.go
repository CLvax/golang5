package main

import (
	"fmt"
)

var scores = [][]int{
	{88, 88, 90, 100},
	{66, 99, 94, 69},
	{75, 84, 98, 95},
	{93, 77, 66, 78},
}
var dir = []string{
	"数学",
	"语文",
	"英语",
	"物理",
}

//格式化标题
func title() {
	for index := range dir {
		fmt.Printf("%-4v\t", dir[index])
	}
	fmt.Println()
}

//成绩格式化显示成绩
func fraction() {
	for i := range scores {
		for index := range scores[i] {
			fmt.Printf("%-7v", scores[i][index])
		}
		fmt.Println()
	}
}

//统计分数
func grades(index int) float64 {
	//各科总分
	var nums float64
	for i := range scores {
		nums += float64(scores[i][index])
	}
	//各科平均分
	return nums / float64(len(scores))
}

func main() {
	//测试 新增学员分数
	scores = append(scores, []int{10, 20, 30, 40})

	//打印成绩
	title()
	fraction()

	//各科平均分
	fmt.Println("各科平均分：")
	title()
	for index := range scores[0] {
		//获取切片索引数
		fmt.Printf("%-7.2f", grades(index))
	}
	fmt.Println()
}

// 记得最后的换行
