package main

import (
	"fmt"
	"unsafe"
)

type Book struct {
	Title  string
	Author string
	Page   int
	Tags   []string
}

// 通过内存地址访问Tag的值
func main() {
	book := Book{
		Tags: []string{"abc", "def", "hjk"},
	}
	b := &book
	tags := *(*[]string)(unsafe.Pointer(uintptr(unsafe.Pointer(b)) + unsafe.Offsetof(b.Tags)))
	tags0 := *(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&b.Tags[0]))))
	tags1 := *(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&b.Tags[0])) + unsafe.Sizeof(b.Tags[0])))
	tags2 := *(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&b.Tags[0])) + unsafe.Sizeof(b.Tags[0]) + unsafe.Sizeof(b.Tags[1])))
	fmt.Printf("Tags: %s, Tags[0]: %s,Tags[1]: %s, Tags[2]: %s\n", tags, tags0, tags1, tags2)
}
