package main

import "fmt"

// 使用结构体表示班级和学生，请计算每个班级学科平均分
// Student 名称(Name) 学号(Number) 科目(Subjects) 成绩(Scores)
// Class   名称(Name) 编号(Number) 学员(Students)
// Class   实现一个平均值的方法

type Class struct {
	Name     string
	Number   int
	Students []*Student
}

type Student struct {
	Name   string
	Number int
	Scores map[string]float32
}

func (c *Class) avg(Scores []float32) (ageScope float32) {
	var sum float32
	for _, n := range Scores {
		sum += n
	}
	ageScope = sum / float32(len(Scores))

	return ageScope
}

func main() {
	ClassA := Class{
		Name:   "ClassA",
		Number: 001,
		Students: []*Student{
			&Student{
				Name:   "张三",
				Number: 001,
				Scores: map[string]float32{
					"Chinese": 125,
					"Math":    134,
					"English": 139,
				},
			},
			&Student{
				Name:   "李四",
				Number: 002,
				Scores: map[string]float32{
					"Chinese": 123,
					"Math":    139,
					"English": 139,
				},
			},
			&Student{
				Name:   "王麻子",
				Number: 003,
				Scores: map[string]float32{
					"Chinese": 68,
					"Math":    89,
					"English": 39,
				},
			},
		},
	}

	Scopes := make(map[string][]float32)
	for _, std := range ClassA.Students {
		Scopes["Chinese"] = append(Scopes["Chinese"], std.Scores["Chinese"])
		Scopes["Math"] = append(Scopes["Math"], std.Scores["Math"])
		Scopes["English"] = append(Scopes["English"], std.Scores["English"])
	}
	ClassAChineseAge := ClassA.avg(Scopes["Chinese"])
	ClassAMathAge := ClassA.avg(Scopes["Math"])
	ClassAEnglishAge := ClassA.avg(Scopes["English"])
	fmt.Printf("ClassA ChineseAge: %f, MathAge: %f, EnglishAge: %f\n", ClassAChineseAge, ClassAMathAge, ClassAEnglishAge)
}
