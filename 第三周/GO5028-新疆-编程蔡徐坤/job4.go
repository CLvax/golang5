package main

import "fmt"

type Student struct {
	Name     string
	Number   int
	Subjects []string
	Scores   []float64
}

type Class struct {
	Name     string
	Number   int
	Students []Student
}

func avg() {
	class1 := Class{
		Name:   "1班",
		Number: 1,
		Students: []Student{
			Student{
				Name:     "ccc",
				Number:   1,
				Subjects: []string{"语文", "数学", "英语"},
				Scores:   []float64{80.0, 78.0, 60.0},
			},
			Student{
				Name:     "xxz",
				Number:   1,
				Subjects: []string{"语文", "数学", "英语"},
				Scores:   []float64{82.0, 72.0, 64.0},
			},
		},
	}

	s := make(map[string][]float64)

	for _, v := range class1.Students {
		s["语文"] = append(s["语文"], v.Scores[0])
		s["数学"] = append(s["数学"], v.Scores[1])
		s["英语"] = append(s["英语"], v.Scores[2])
	}

	tot := 0.0
	for _, v := range s {
		for _, v1 := range v {
			tot += v1

		}
	}
	fmt.Println(tot / 3)
}

func main() {
	avg()
}


// 每个班级的学科平均分