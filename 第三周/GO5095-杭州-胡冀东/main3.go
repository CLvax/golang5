package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

type Student struct {
	Name     string
	Number   int
	Subjects []string
	Scores   []int
}
type Class struct {
	Name     string
	Number   int
	Students []*Student
}

//感觉这里还是使用切片指针效率会好些，本质是指针类型的数组 最最本质就是[]int切片 最最最本质就是int型的动态数组
//本质上数组都是静态的 数不尽的malloc() + free()造就了错觉上的动态数组 啦啦啦
//等以后学完了再回头看看自己的理解是否是欠妥的

func initaClass(classA *Class) { //传入一个Class的指针 进行初始化
	rand.Seed(time.Now().UnixNano())

	for i := 0; i <= 49; i++ { //一个班要有50个学生吧
		classA.Students = append(classA.Students, new(Student))
		(*classA.Students[i]).Number = i + 1                                                             //定义学生的学号
		(*classA.Students[i]).Name = "student" + strconv.Itoa(i+1)                                       //定义学生Name
		(*classA.Students[i]).Subjects = []string{"语文", "数学", "英语"}                                      //三门课意思意思
		(*classA.Students[i]).Scores = []int{rand.Intn(70) + 80, rand.Intn(70) + 80, rand.Intn(70) + 80} //随机生成分数 保底80份 满分150
		//fmt.Println((*classA.Students[i]).Name, (*classA.Students[i]).Number, (*classA.Students[i]).Subjects, (*classA.Students[i]).Scores) //打印下学生Name 跟 Number 科目和分数
	}

}
func printClassScores(classA *Class) {
	for i := 0; i < len((*classA).Students); i++ {
		fmt.Println("学生Name:", (*classA.Students[i]).Name, "学生学号:", "科目:", (*classA.Students[i]).Number, (*classA.Students[i]).Subjects, "成绩:", (*classA.Students[i]).Scores)
	}

}
func classScoreAve(classA *Class) (a, b, c int) {
	//接受一个Class的指针 返回各个科目平均值 其实题目的定义还是有些问题 比如我觉得科目应该定义在CLASS当中
	//目前科目定义在student中 感觉数据有大量不必要的冗余（当然 如果是大学生的话有自己的科目是正常的）
	//或许返回值应该定义成一个int类型的切片扩展性更高点但是由于取科目数据是从学生抽取的，逻辑上有些过不去
	//就这样吧 意思意思
	for i := 0; i < len((*classA).Students); i++ {
		//println(i)
		a += (*classA.Students[i]).Scores[0]
		b += (*classA.Students[i]).Scores[1]
		c += (*classA.Students[i]).Scores[2]
	}
	a = a / len((*classA).Students)
	b = b / len((*classA).Students)
	c = c / len((*classA).Students)
	return

}

func main() {

	classA := new(Class)
	classA.Name = "ClassA"
	classA.Number = 101
	initaClass(classA)
	printClassScores(classA)
	a, b, c := classScoreAve(classA)
	fmt.Printf("班级语文平均成绩%d\n班级数学平均成绩%d\n班级英语平均成绩%d\n", a, b, c)
	//fmt.Println(classA)
}

// 直接计算出结果即可