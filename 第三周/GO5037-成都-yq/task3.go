package main

// 使用结构体表示班级和学生，请计算每个班级学科平均分
// Student 名称(Name) 学号(Number) 科目(Subjects) 成绩(Scores)
// Class   名称(Name) 编号(Number) 学员(Students)
// Class   实现一个平均值的方法
type Student struct {
	Name     string
	Number   string
	Subjects map[string]int
	Scores   int
}

type Class struct {
	Name   string
	Number string
	Student
}

func (c *Class) Avg(subject string) float64 {

}
