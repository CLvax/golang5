package main

import (
	"fmt"
)

// 使用二维切片表示一组学生的各科成绩，计算所有学生的平均分
//  数学   语文   英语
//   88     88    90
//   66
//   ...
//   avg

func main() {
	scores := [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}

	var mathTotal, chineseTotal, englishTotal int
	for _, student := range scores {
		mathTotal += student[0]
		chineseTotal += student[1]
		englishTotal += student[2]
	}
	fmt.Printf("语文平均分: %.2f\n", float64(mathTotal)/float64(len(scores)))
	fmt.Printf("数学平均分: %.2f\n", float64(chineseTotal)/float64(len(scores)))
	fmt.Printf("英语平均分: %.2f\n", float64(englishTotal)/float64(len(scores)))
}
