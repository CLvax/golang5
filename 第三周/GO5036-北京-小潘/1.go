package main

import (
	"fmt"
	"unsafe"
)

const (
	N = unsafe.Offsetof(Book{}.Tag)
	//M = unsafe.Sizeof(Book{}.Tag[0])
)

type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

func main() {
	b := &Book{Tag: []string{"abc", "def", "hjnk"}}

	fmt.Println(*(*[]string)(unsafe.Pointer(uintptr(unsafe.Pointer(b)) + N))) //[abc def hjnk]

	fmt.Println(*(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&b.Tag[0])))))                                                     //abc
	fmt.Println(*(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&b.Tag[0])) + unsafe.Sizeof(b.Tag[0]))))                           //def
	fmt.Println(*(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&b.Tag[0])) + unsafe.Sizeof(b.Tag[0]) + unsafe.Sizeof(b.Tag[0])))) //hjnk
}

