package main

import (
	"fmt"
)

type Student struct {
	Name     string
	Number   int
	Subjects []string
	Scores   []int
}

type Class struct {
	Name    string
	Number  int
	Student []Student
}

func main() {

	s1 := Student{
		Name:     "zhangsan",
		Number:   1,
		Subjects: []string{"math", "english", "chinese"},
		Scores:   []int{88, 88, 90},
	}

	s2 := Student{
		Name:     "lisi",
		Number:   2,
		Subjects: []string{"math", "english", "chinese"},
		Scores:   []int{66, 99, 94},
	}

	s3 := Student{
		Name:     "wangwu",
		Number:   3,
		Subjects: []string{"math", "english", "chinese"},
		Scores:   []int{75, 84, 98},
	}

	s4 := Student{
		Name:     "wangwu",
		Number:   4,
		Subjects: []string{"math", "english", "chinese"},
		Scores:   []int{88, 88, 90},
	}

	c1 := Class{
		Name:    "class1",
		Number:  1,
		Student: []Student{s1, s2},
	}

	c2 := Class{
		Name:    "class2",
		Number:  2,
		Student: []Student{s3, s4},
	}

	c1.avg()
	c2.avg()

}

func (c Class) avg() {
	math1, eng1, cn1 := 0, 0, 0

	for _, v := range c.Student {
		math1 += v.Scores[0]
		eng1 += v.Scores[1]
		cn1 += v.Scores[2]
	}
	fmt.Printf("%v 班级的成绩为如下：\n",c.Name)
	fmt.Printf("数学平均分为：%d\n", math1/len(c.Student))
	fmt.Printf("英语平均分为：%d\n", eng1/len(c.Student))
	fmt.Printf("语文平均分为：%d\n", cn1/len(c.Student))
	fmt.Println()
}

