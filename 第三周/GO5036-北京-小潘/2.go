package main

import (
	"fmt"
)

func main() {

	scores := [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}

	math := 0
	en := 0
	ch := 0
	for _, v := range scores {
		math += v[0]
		ch += v[1]
		en += v[2]
	}

	fmt.Printf("数学平均分为：%d\n", math/len(scores))
	fmt.Printf("英语平均分为：%d\n", en/len(scores))
	fmt.Printf("语文平均分为：%d\n", ch/len(scores))

}

