package main

import "fmt"

//使用结构体表示班级和学生，请计算每个班级学科平均分
// Student 名称(Name) 学号(Number) 科目(Subjects) 成绩(Scores)
// Class   名称(Name) 编号(Number) 学员(Students)
// Class   实现一个平均值的方法
type Student struct {
	Name string
	Number int
	Subjects []string
	Scores []int
}

type Class struct {
	Name string
	Number int
	Students []Student
}

func main() {
	//fmt.Println(s1)
	c1 :=&Class{
		Name: "1班",
		Number: 2,
		Students: []Student{
			Student{
				Name: "LiHong",
				Number: 1,
				Subjects: []string{"Chinese","Math","English"},
				Scores: []int{90,43,78},
			},
			Student{
				Name: "ZhangSan",
				Number: 2,
				Subjects: []string{"Chinese","Math","English"},
				Scores: []int{79,72,23},
			},
		},
	}


	c2 :=&Class{
		Name: "2班",
		Number: 2,
		Students: []Student{
			Student{
				Name: "LiMing",
				Number: 1,
				Subjects: []string{"Chinese","Math","English"},
				Scores: []int{90,78,78},
			},
			Student{
				Name: "LiSi",
				Number: 2,
				Subjects: []string{"Chinese","Math","English"},
				Scores: []int{79,72,93},
			},
		},
	}

	var (
		SumChinses int
		SumMath int
		SumEnglish int
	)



	for i:=0;i<2;i++{
		//fmt.Println(c1.Students[i].Scores)
		for j:=0;j<3;j++{
			//fmt.Println(c1.Students[i].Scores[j])
			if j==0{
				SumChinses += c1.Students[i].Scores[j]
			}
			if j==1{
				SumMath += c1.Students[i].Scores[j]
			}
			if j==2{
				SumEnglish += c1.Students[i].Scores[j]
			}
		}

	}
	fmt.Printf("%s平均分 语文：%d 数学；%d 英语：%d\n",c1.Name,SumChinses/2,SumMath/2,SumEnglish/2)


	for i:=0;i<2;i++{
		SumChinses,SumMath,SumEnglish = 0,0,0
		for j:=0;j<3;j++{
			//fmt.Println(c1.Students[i].Scores[j])
			if j==0{
				SumChinses += c2.Students[i].Scores[j]
			}
			if j==1{
				SumMath += c2.Students[i].Scores[j]
			}
			if j==2{
				SumEnglish += c2.Students[i].Scores[j]
			}
		}

	}
	fmt.Printf("%s平均分 语文：%d 数学；%d 英语：%d\n",c2.Name,SumChinses/2,SumMath/2,SumEnglish/2)
}

