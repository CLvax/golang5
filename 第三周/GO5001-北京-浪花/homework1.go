package main

import (
	"fmt"
	"unsafe"
)

// 1.通过内存地址访问Tag的值
type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

func main() {
	b := Book{Tag: []string{"abc", "def", "hjk"}}
	p1 := unsafe.Pointer(&b.Tag[0])
	p2 := unsafe.Pointer(&b.Tag[1])
	p3 := unsafe.Pointer(&b.Tag[2])
	v1 :=(*string)(unsafe.Pointer(uintptr(p1)))
	v2 :=(*string)(unsafe.Pointer(uintptr(p2)))
	v3 :=(*string)(unsafe.Pointer(uintptr(p3)))
	fmt.Println(p1,p2,p3)
	fmt.Println(*v1,*v2,*v3)

}
