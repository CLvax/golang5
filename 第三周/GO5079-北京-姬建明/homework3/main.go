package main

import "fmt"

/*
使用结构体表示班级和学生，请计算每个班级学科平均分
Student 名称(Name) 学号(Number) 科目(Subjects) 成绩(Scores)
Class   名称(Name) 编号(Number) 学员(Students)
Class   实现一个平均值的方法

*/
type Student struct {
	Name     string
	Number   int
	Subjects string
	Scores   float64
}

type Class struct {
	Name    string
	Number  int
	Student []Student
}

func (c *Class) average() float64 {
	var total float64
	for _, value := range c.Student {
		total += value.Scores
	}
	return total / float64(len(c.Student))
}
func main() {
	c1 := Class{
		Name:   "三年级",
		Number: 1,
		Student: []Student{
			{
				Name:     "张三",
				Number:   1000,
				Subjects: "数学",
				Scores:   96.5,
			},
			{
				Name:     "李四",
				Number:   1001,
				Subjects: "数学",
				Scores:   91.5,
			},
		},
	}
	fmt.Printf("班级:%s的数学平均分为：%.2f\n", c1.Name, c1.average())
}
