package main

import "fmt"

/*
使用二维切片表示一组学生的各科成绩，计算所有学生的平均分
 数学   语文   英语
  88     88    90
  66
  ...
  avg
scores = [][]int{
	{88, 88, 90},
	{66, 99, 94},
	{75, 84, 98},
	{93, 77, 66},
}

*/
func main() {
	scores := [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}
	var total_sx, total_yw, total_yy int
	for _, value := range scores {
		total_sx += value[0]
		total_yw += value[1]
		total_yy += value[2]
	}
	fmt.Printf("数学的平均分为:{%.2f}\n", float64(total_sx)/float64(len(scores)))
	fmt.Printf("语文的平均分为:{%.2f}\n", float64(total_yw)/float64(len(scores)))
	fmt.Printf("英语的平均分为:{%.2f}\n", float64(total_yy)/float64(len(scores)))
}
