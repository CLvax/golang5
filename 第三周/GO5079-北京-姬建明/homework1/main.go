package main

import (
	"fmt"
	"unsafe"
)

/*
通过内存地址访问Tag的值
type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

b := &Book{Tag: []string{"abc", "def", "hjk"}}
根据结构体的内存地址, 计算出Tag的内存地址, 并访问

*/

type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

func main() {
	b1 := &Book{
		Title:  "Golang",
		Author: "laoyu",
		Page:   300,
		Tag:    []string{"Golang", "Python", "Vue"},
	}
	// 先求出b1的Poninter值利用uintptr计算加上b1.Tag的偏移量就能获取到b1.tag的地址，然后换成Pointer类型
	tag := (*[]string)(unsafe.Pointer(uintptr(unsafe.Pointer(b1)) + unsafe.Offsetof(b1.Tag)))
	fmt.Printf("结构体Book的Tag字段的内存地址为:%p,\n", tag)
	for index, value := range *tag {
		fmt.Printf("Tag[%d]的值为%s\n", index, value)
	}

}
