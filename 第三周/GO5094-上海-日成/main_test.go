package main

import (
	"fmt"
	"testing"
	"unsafe"
)

type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

func TestMain1(t *testing.T) {

	b := Book{
		Tag: []string{"abc", "def", "hjk"},
	}
	// p := uintptr(unsafe.Pointer(&b.Tag[0]))
	fmt.Println(*(*[]string)(unsafe.Pointer(uintptr(unsafe.Pointer(&b)) + unsafe.Offsetof(b.Tag))))
	fmt.Println(*(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&b.Tag[0])))))
	fmt.Println(*(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&b.Tag[1])))))
	fmt.Println(*(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&b.Tag[2])))))
}

func TestMain2(t *testing.T) {

	scores := [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}
	for _, v := range scores {
		sum := 0
		for i, j := range v {
			sum += j
			if i == len(v)-1 {
				fmt.Printf("%4.2f\n", float64(sum)/float64(len(v)))
			}
		}
	}
}

func TestMain3(t *testing.T) {
	c := Class{
		Name:   "一班",
		Number: "1",
		Students: []Student{
			Student{
				Name:     "张三",
				Number:   "00001",
				Subjects: []string{"数学", "语文", "英语", "化学"},
				Scores:   []float64{78, 90, 89, 90},
			},
			Student{
				Name:     "李四",
				Number:   "00002",
				Subjects: []string{"数学", "语文", "英语", "化学"},
				Scores:   []float64{22, 90, 89, 90},
			},
			Student{
				Name:     "王五",
				Number:   "00003",
				Subjects: []string{"数学", "语文", "英语", "化学"},
				Scores:   []float64{18, 90, 89, 98},
			},
		},
	}
	m := c.TotalScore()
	fmt.Printf("数学平均分为%4.2f\n", m["数学"])
	fmt.Printf("语文平均分为%4.2f\n", m["语文"])
	fmt.Printf("英语平均分为%4.2f\n", m["英语"])
	fmt.Printf("化学平均分为%4.2f\n", m["化学"])
}

type Student struct {
	Name     string
	Number   string
	Subjects []string
	Scores   []float64
}

type Class struct {
	Name     string
	Number   string
	Students []Student
}

func (c Class) TotalScore() (m map[string]float64) {
	student_number := len(c.Students)
	m = make(map[string]float64)
	for i := 0; i < student_number; i++ {
		for k, v := range c.Students[i].Subjects {
			m[v] += c.Students[i].Scores[k]
		}
	}
	for _, v := range c.Students[0].Subjects {
		m[v] = m[v] / float64(student_number)
	}
	return
}
