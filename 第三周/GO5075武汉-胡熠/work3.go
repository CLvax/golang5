package main

import "fmt"

type Student struct {
	name     string    // 学生姓名
	Number   int       // 学号
	Subjects []string  // 科目
	Scores   []float64 // 分数
}

type Class struct {
	Name    string    // 班级
	Number  int       // 编号
	student []Student // 学生信息
}

func myAvg(c *Class) {
	//求成绩的平均分

	lens := 0 // 课程数
	js := 0.0 // 计算机基础初始值
	gs := 0.0 // 高数初始值
	for index, value := range c.student {
		js = js + value.Scores[0]
		gs = gs + value.Scores[1]
		lens = index + 1

	}
	fmt.Printf("计算机基础的平均分: %v\n", js/float64(lens))
	fmt.Printf("高数的平均分: %v\n", gs/float64(lens))

	// result = sum / float64(lens+1)
	// return

}

func main() {
	p3 := &Class{
		Name:   "软件学院一班",
		Number: 7018,
		student: []Student{
			{
				name:     "李明",
				Number:   1,
				Subjects: []string{"计算机基础", "高数"},
				Scores:   []float64{85.5, 96},
			},
			{
				name:     "赵强",
				Number:   2,
				Subjects: []string{"计算机基础", "高数"},
				Scores:   []float64{80.5, 99},
			},
		},
	}
	myAvg(p3)
}
