package main

import "fmt"

func arrayAvg(myScoresArray [][]int) (result float32) {
	/*
		求各科成绩的平均值
	*/
	sum := 0
	for _, myScores := range myScoresArray {
		sum += myScores[0]
	}
	result = float32(sum) / 3
	return
}

func main() {
	scores := [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}

	fmt.Println(arrayAvg(scores))
}
