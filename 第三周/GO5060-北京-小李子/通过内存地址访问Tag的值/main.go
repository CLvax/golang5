package main

import (
	"fmt"
	"unsafe"
)

type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

func main() {
	book := Book{Tag: []string{"你好", "世界", "!"}}
	t1 := unsafe.Sizeof(Book{}.Tag[0])
	rest1 := (*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&book.Tag[0]))))
	rest2 := (*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&book.Tag[0])) + t1))
	rest3 := (*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&book.Tag[1])) + t1))
	fmt.Println(*rest1)
	fmt.Println(*rest2)
	fmt.Println(*rest3)
}

// 还有内存地址