package main

import "fmt"

func avg() float64 {
	//  数学   语文   英语
	//   88     88    90
	//   66
	//   ...
	//   avg
	scores := [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}
	//遍历切片将所有分数取出存放在sc里
	sc := []int{}
	for _, v := range scores {
		for _, v1 := range v {
			sc = append(sc, v1)
		}
	}
	//计算总分
	sum := 0
	for _, v := range sc {
		sum  += v
	}

	return float64(sum) / float64(len(scores))
}

func main() {
	fmt.Println("学生平均分：", avg())
}
