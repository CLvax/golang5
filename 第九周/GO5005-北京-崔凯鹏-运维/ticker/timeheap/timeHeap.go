package timeheap

import (
	"time"
)

type DateHeap struct {
	Date time.Time
	Data string
}

// NewDateHeap
func NewDateHeap(seconds int, data string) *DateHeap {
	t := time.Now().Add(time.Duration(seconds) * time.Second)
	return &DateHeap{
		Date: t,
		Data: data,
	}
}

// An IntHeap is a min-heap of ints.
type IntHeap []*DateHeap

func (h IntHeap) Len() int           { return len(h) }
func (h IntHeap) Less(i, j int) bool { return h[i].Date.Unix() < h[j].Date.Unix() }
func (h IntHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *IntHeap) Push(x interface{}) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	*h = append(*h, x.(*DateHeap))
}

func (h *IntHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}
