package timeheap_test

import (
	"container/heap"
	"fmt"
	"testing"
	"ticker01/timeheap"

	"github.com/stretchr/testify/assert"
)

func TestHeap(t *testing.T) {
	assert := assert.New(t)
	d1 := timeheap.NewDateHeap(200, "200")
	d2 := timeheap.NewDateHeap(300, "300")
	d3 := timeheap.NewDateHeap(100, "100")
	d4 := timeheap.NewDateHeap(20, "20")
	h := &timeheap.IntHeap{d3, d4}
	heap.Init(h)
	heap.Push(h, d1)
	heap.Push(h, d2)

	ans := []string{
		"20",
		"100",
		"200",
		"300",
	}
	i := 0
	for h.Len() > 0 {
		b := heap.Pop(h)
		// fmt.Printf("%#v \n ",b)
		b1, _ := b.(*timeheap.DateHeap)
		assert.Equal(b1.Data, ans[i])
		fmt.Println(b1)
		i++

	}
}
