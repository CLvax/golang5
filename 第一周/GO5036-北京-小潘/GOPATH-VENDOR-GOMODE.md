#### GOPATH
##### 默认目录

```
~/go(unix，linux)
%USERPROFILE%go(win)
```
##### 解释说明
所有依赖都到 gopath下面来找，这就会导致所有项目都放到GOPATH目录下，所有依赖第三方的库也放到GOPATH目录下，gopath目录会越来越大

##### 使用举例
```
$ env|grep -i gopath
GOPATH=/Users/panyinyue/go
# 项目和依赖库都在src目录下，还需注意当使用gopath时需要配置 GO111MODULE="off"，通过 go env -w GO111MODULE=off
 ls /Users/panyinyue/go/src/
# 下载依赖包测试，查看下载后存放位置
~/go/src/GOGO$ go get -u go.uber.org/zap
~/go/src/GOGO$ ls ~/go/src/go.uber.org/

# import 导入包会去 ~/go/src/下面去找
package main

import (
	"go.uber.org/zap"
)

func main() {
	log, _ := zap.NewProduction()
	log.Warn("hello world")
}

```
### vendor
如果src 下面有两个项目，且两个项目都zap这个包的不同版本有依赖是gopath没办法解决的，因此出现了vendor，在不通的项目下面建一个vendor目录~/go/src/project1/vendor, 查找关系是先找vendor 然后去外面src 下面去找，这样就解决了，不同项目对不同版本包的依赖

##### vendor管理工具
```
glide、dep，go dep
```

### go mod
go mod 用户在也不用关心目录结构了

##### 使用举例
```
# 启用需要进入到项目目录里面进行初始化，然后多出来一个 go.mod文件
panyinyue@panyinyues-MacBook-Pro:~/go/src/GOGO$ go mod init GOGO
go: creating new go.mod: module GOGO
go: to add module requirements and sums:
        go mod tidy
panyinyue@panyinyues-MacBook-Pro:~/go/src/GOGO$ ls
go.mod  main.go

```


```
# 下载依赖包
panyinyue@panyinyues-MacBook-Pro:~/go/src/GOGO$ go get -u go.uber.org/zap
go get: added go.uber.org/multierr v1.7.0
go get: added go.uber.org/zap v1.17.0

# 此时 下载的包所在路径为
panyinyue@panyinyues-MacBook-Pro:~/go/src/GOGO$ ls ~/go/pkg/mod/go.uber.org/ 
atomic@v1.4.0/   atomic@v1.7.0/   multierr@v1.5.0/ multierr@v1.7.0/ zap@v1.15.0/     zap@v1.17.0/     
atomic@v1.6.0/   multierr@v1.1.0/ multierr@v1.6.0/ zap@v1.10.0/     zap@v1.16.0/

#使用其他版本，切换版本之后 go.mod文件内容会变成1.11
go get -u go.uber.org/zap@v1.11

#清除 go.sum中老的版本
go mod tidy

# 获取最新版本
go get -u go.uber.org/zap

# 将整个项目的依赖都下载来下
go  build ./...
```
