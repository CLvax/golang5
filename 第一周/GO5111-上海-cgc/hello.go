package main

import "fmt"

func main() {
	/* my first programm */
	a := "i am cgc"
	b := "line1\nline2\nline3"
	var c string
	c = "string c"
	fmt.Println("hello,world", a, b, &c)
	fmt.Println(PointerTest(&a))

	var slice3 []string
	slice3 = make([]string, 3, 10)
	slice3 = append(slice3, "a", "b", "c")
	var slice5 []int
	slice5 = []int{1, 2, 3, 4}
	fmt.Println(slice3)
	var mylen int
	mylen = len(slice3)
	fmt.Println(mylen)
	slice4 := slice5[1:]
	fmt.Println("qiepian", slice5[1:3], slice4)

	var map1 = map[string]string{"key1": "1"}
	map1["key2"] = "2"
	fmt.Println(map1)
	for k, v := range map1 {
		fmt.Println(k, v)
	}

}

func PointerTest(a *string) string {
	*a += "string b"
	return *a
}
