#  GoPath和Go Module

## 1.1 GoPath 介绍

GOPATH 是 Go语言中使用的一个环境变量，它使用绝对路径提供项目的工作目录。

在 Go 1.8 版本之前，GOPATH 环境变量默认是空的。从 Go 1.8 版本开始，Go 开发包在安装完成后，将 GOPATH 赋予了一个默认的目录，参见下表。

**GOPATH在不同操作系统平台上的默认值**

|  平台   |   GOPATH默认值   |        举例        |
| :-----: | :--------------: | :----------------: |
| Windows | %USERPROFILE%/go | C:\Users\用户名\go |
|  Unix   |     $HOME/go     |  /home/用户名/go   |

GOPATH告知go，需要代码(包括本项目即内部依赖和引用外部项目的代码即外部依赖)的时候去哪里找。GOPATH随着项目的不同重新设置

## 1.2 GOPATH 目录结构

 GOPAT有三个目录：`src`  `bin`  `pkg`

- src目录：存储所有 `.go` 文件或源代码。在编写Go应用程序、程序包和库时，一般会以`$GOPATH/src/github.con/foo/bar` 的路径进行存放
- bin目录：go get 这种bin工具的时候，二进制文件下载的目的地；golang 编译可执行文件存放路径
- pkg目录：golang编译包时，生成的中间缓存文件(.a)存放路径。中间缓存文件(.a)是编译过程中生成的，每个package都会生成对应的中间缓存文件(.a)，Go在编译的时候先判断package的源码是否有改动，如果没有的话，就不再重新编译中间缓存文件(.a)。
  

## 1.3 为什么弃用 GOPATH 模式

在 GOPATH 的 `$GOPATH/src` 下进行 ` .go` 文件或源代码的存储，我们可以称其为 GOPATH 的模式，这个模式，看起来好像没有什么问

题，那么为什么我们要弃用呢，参见如下原因：

**GOPATH 模式下没有版本控制的概念，具有致命的缺陷，至少会造成以下问题：**

- 在执行 `go get` 的时候，你无法传达任何的版本信息的期望，也就是说你也无法知道自己当前更新的是哪一个版本，也无法通过指定来拉取自己所期望的具体版本。

- 在运行 Go 应用程序的时候，你无法保证其它人与你所期望依赖的第三方库是相同的版本，也就是说在项目依赖库的管理上，你无法保证所有人的依赖版本都一致。

- 你没办法处理 v1、v2、v3 等等不同版本的引用问题，因为 GOPATH 模式下的导入路径都是一样的，都是`github.com/foo/bar`。

**Go 语言官方从 Go1.11 起开始推进 Go modules（前身vgo），Go1.13 起不再推荐使用 GOPATH 的使用模式，Go modules 也渐趋稳定，因此新项目也没有必要继续使用GOPATH模式。**

从 Go1.17 开始，Go1.17 就将 GOPATH 从编译器工具链中移除 `GO111MODULE` 标识。

提案地址 https://github.com/golang/go/issues/44519

博客地址 https://blog.golang.org/go116-module-changes

![drop_GOPATH](https://51k8s.oss-cn-shenzhen.aliyuncs.com/golang/images/drop_GOPATH.png)

## 1.4 在 GOPATH 模式下的产物

Go1 在 2012 年 03 月 28 日发布，而 Go1.11 是在 2018 年 08 月 25 日才正式发布（数据来源：GitHub Tag），在这个空档的时间内，并没有 Go modules 这一个东西，最早期可能还好说，因为刚发布，用的人不多，所以没有明显暴露，但是后期 Go 语言使用的人越来越多了，那怎么办？

这时候社区中逐渐的涌现出了大量的依赖解决方案，百花齐放，让人难以挑选，其中包括我们所熟知的 vendor 目录的模式，以及曾经一度被认为是“官宣”的 dep 的这类依赖管理工具。但为什么 dep 没有正在成为官宣呢，其实是因为随着 Russ Cox 与 Go 团队中的其他成员不断深入地讨论，发现 dep 的一些细节似乎越来越不适合 Go，因此官方采取了另起 proposal 的方式来推进，其方案的结果一开始先是释出 vgo（Go modules 的前身，知道即可，不需要深入了解），最终演变为我们现在所见到的 Go modules，也在 Go1.11 正式进入了 Go 的工具链。因此与其说是 “在 GOPATH 模式下的产物”，不如说是历史为当前提供了重要的教训，因此出现了 Go modules。



## 1.5 了解Go Modules

Go Modules历史

在过去，Go 的依赖包管理在工具上混乱且不统一，有 dep，有 glide，有 govendor…甚至还有因为外网的问题，频频导致拉不下来包，很多人苦不堪言，盼着官方给出一个大一统做出表率。

而在 Go modules 正式出来之前还有一个叫 dep 的项目，我们在上面有提到，它是 Go 的一个官方实验性项目，目的也是为了解决 Go 在依赖管理方面的问题，当时社区里面几乎所有的人都认为 dep 肯定就是未来 Go 官方的依赖管理解决方案了。

但是万万没想到，半路杀出个程咬金，Russ Cox 义无反顾地推出了 Go  modules，这瞬间导致一石激起千层浪，让社区炸了锅。大家一致认为 Go team  实在是太霸道、太独裁了，连个招呼都不打一声。我记得当时有很多人在网上跟 Russ Cox  口水战，各种依赖管理解决方案的专家都冒出来发表意见，讨论范围甚至一度超出了 Go 语言的圈子触及到了其他语言的领域。

当然，最后，推成功了，Go modules 已经进入官方工具链中，与 Go 深深结合，以前常说的 GOPATH 终将会失去它原有的作用，而且它还提供了 GOPROXY 间接解决了国内访问外网的问题。

## 1.6  Go Modules 命令

在 Go modules 中，我们能够使用如下命令进行操作：

|      命令       |               作用               |
| :-------------: | :------------------------------: |
|   go mod ini    |         生成 go.mod 文件         |
| go mod download | 下载 go.mod 文件中指明的所有依赖 |
|   go mod tidy   |          整理现有的依赖          |
|  go mod graph   |        查看现有的依赖结构        |
|   go mod edit   |         编辑 go.mod 文件         |
|  go mod vendor  |  导出项目所有的依赖到vendor目录  |
|  go mod verify  |     校验一个模块是否被篡改过     |
|   go mod why    |     查看为什么需要依赖某模块     |

## 1.7 GO Modules 提供的环境变量

在 Go modules 中有如下常用环境变量，我们可以通过 go env 命令来进行查看，如下：

```
$ go env 
GO111MODULE=on
GOPROXY=https://goproxy.cn,direct
GONOPROXY=
GOSUMDB=sum.golang.org
GONOSUMDB=
GOPRIVATE=
```

### 1.7.1 GO111MODULE

go modules 是 golang 1.11 新加的特性

GO111MODULE 有三个值：off, on和auto（默认值）。

- GO111MODULE=off，无模块支持，go命令行将不会支持module功能，寻找依赖包的方式将会沿用旧版本那种通过vendor目录或者GOPATH模式来查找。
- GO111MODULE=on，模块支持，go命令行会使用modules，而一点也不会去GOPATH目录下查找。
- GO111MODULE=auto，默认值(go 1.16开始默认值为on)，go命令行将会根据当前目录来决定是否启用module功能。

这种情况下可以分为两种情形：
1.当前目录在GOPATH/src之外且该目录包含go.mod文件，开启模块支持。
2.当前文件在包含go.mod文件的目录下面

> 在使用模块时，GOPATH是无意义的，不过它还是会把下载的依赖存储在$GOPATH/pkg/mod 中，也会把go install 的结果放在 $GOPATH/bin 中。
>
> 当modules 功能启用时，依赖包的存放位置变更为$GOPATH/pkg，允许同一个package多个版本并存，且多个项目可以共享缓存的module。

**GO111MODULE 小历史**

你可能会留意到 GO111MODULE 这个名字比较“奇特”，实际上在 Go 语言中经常会有这类阶段性的变量， GO111MODULE 这个命名代表着Go语言在 1.11 版本添加的，针对 Module 的变量。
像是在 Go1.5 版本的时候，也发布了一个系统环境变量 GO15VENDOREXPERIMENT，作用是用于开启 vendor 目录的支持，当时其默认值也不是开启，仅仅作为 experimental。其随后在 Go1.6 版本时也将默认值改为了开启，并且最后作为了official，GO15VENDOREXPERIMENT 系统变量就退出了历史舞台。
而未来 GO111MODULE 这一个系统环境变量也会面临这个问题，也会先调整为默认值为 on（曾经在Go1.13想想改为 on，并且已经合并了 PR，但最后因为种种原因改回了 auto），然后再把 GO111MODULE 的支持给去掉，我们猜测应该会在 Go2 将 GO111MODULE 给去掉，因为如果直接去掉 GO111MODULE 的支持，会存在兼容性问题。 

### 1.7.2 GOPROXY

这个环境变量主要是用于设置 Go 模块代理（Go module proxy），其作用是用于使 Go 在后续拉取模块版本时能够脱离传统的 VCS 方式，直接通过镜像站点来快速拉取。

GOPROXY 的默认值是：https://proxy.golang.org,direct，这有一个很严重的问题，就是 `proxy.golang.org` 在国内是无法访问的，因此这会直接卡住你的第一步，所以你必须在开启 Go modules 的时，同时设置国内的 Go 模块代理，执行如下命令：

```
go env -w GOPROXY=https://goproxy.cn,direct
```

GOPROXY 的值是一个以英文逗号 “,” 分割的 Go 模块代理列表，允许设置多个模块代理，假设你不想使用，也可以将其设置为 “off” ，这将会禁止 Go 在后续操作中使用任何 Go 模块代理。

**direct是什么**

而在刚刚设置的值中，我们可以发现值列表中有 “direct” 标识，它又有什么作用呢？
实际上 “direct” 是一个特殊指示符，用于指示 Go 回源到模块版本的源地址去抓取（比如 GitHub 等），场景如下：当值列表中上一个 Go 模块代理返回 404 或 410 错误时，Go 自动尝试列表中的下一个，遇见 “direct” 时回源，也就是回到源地址去抓取，而遇见 EOF 时终止并抛出类似 “invalid version: unknown revision...” 的错误。

### 1.7.3 GOSUMDB

它的值是一个 Go checksum database，用于在拉取模块版本时（无论是从源站拉取还是通过 Go module proxy 拉取）保证拉取到的模块版本数据未经过篡改，若发现不一致，也就是可能存在篡改，将会立即中止。

GOSUMDB 的默认值为：sum.golang.org，在国内也是无法访问的，但是 GOSUMDB 可以被 Go 模块代理所代理（详见：Proxying a Checksum Database）。
因此我们可以通过设置 GOPROXY 来解决，而先前我们所设置的模块代理 goproxy.cn 就能支持代理 sum.golang.org，所以这一个问题在设置 GOPROXY 后，你可以不需要过度关心。

另外若对 GOSUMDB 的值有自定义需求，其支持如下格式：
格式 1：<SUMDB_NAME>+<PUBLIC_KEY>。
格式 2：<SUMDB_NAME>+<PUBLIC_KEY> <SUMDB_URL>。
也可以将其设置为“off”，也就是禁止 Go 在后续操作中校验模块版本。

### 1.7.4 GONOPROXY/GONOSUMDB/GOPRIVATE

这三个环境变量都是用在当前项目依赖了私有模块，例如像是你公司的私有 git 仓库，又或是 github 中的私有库，都是属于私有模块，都是要进行设置的，否则会拉取失败。
更细致来讲，就是依赖了由 GOPROXY 指定的 Go 模块代理或由 GOSUMDB 指定 Go checksum database 都无法访问到的模块时的场景。

而一般建议直接设置 GOPRIVATE，它的值将作为 GONOPROXY 和 GONOSUMDB 的默认值，所以建议的最佳姿势是直接使用 GOPRIVATE。
并且它们的值都是一个以英文逗号 “,” 分割的模块路径前缀，也就是可以设置多个，例如：

```
go env -w GOPRIVATE="git.example.com,github.com/eddycjy/mquote"
```

设置后，前缀为 git.xxx.com 和 github.com/eddycjy/mquote 的模块都会被认为是私有模块。

如果不想每次都重新设置，我们也可以利用通配符，例如：

```
go env -w GOPRIVATE="*.example.com"
```

这样子设置的话，所有模块路径为 example.com 的子域名（例如：git.example.com）都将不经过 Go module proxy 和 Go checksum database，需要注意的是不包括 example.com 本身。

## 1.8 go.mod 文件介绍

在初始化项目时，会生成一个 go.mod 文件，是启用了 Go modules 项目所必须的最重要的标识，同时也是 GO111MODULE 值为 auto 时的识别标识，它描述了当前项目（也就是当前模块）的元信息，每一行都以一个动词开头。

```
module github.com/infraboard/cloudstation

go 1.16
require (
    example.com/apple v0.1.3
    example.com/banana v1.2.5
    example.com/banana/v2 v2.3.5
    example.com/pear // indirect
    example.com/strawberry // incompatible
)

exclude example.com/banana v1.2.4
replace example.com/apple v0.1.2 => example.com/fried v0.1.0 
```

- module：用于定义当前项目的模块路径。
- go：用于标识当前模块的 Go 语言版本，值为初始化模块时的版本，目前来看还只是个标识作用。
- require：用于设置一个特定的模块版本。
- exclude：用于从使用中排除一个特定的模块版本。
- replace：用于将一个模块版本替换为另外一个模块版本。

另外你会发现 example.com/pear 的后面会有一个 `indirect`  标识，indirect 标识表示该模块为间接依赖，也就是在当前应用程序中的 import 语句中，并没有发现这个模块的明确引用，有可能是你先手动 go get 拉取下来的，也有可能是你所依赖的模块所依赖的，情况有好几种。

### 1.9 go.sum 文件介绍

**go.sum 文件**

在第一次拉取模块依赖后，会发现多出了一个 go.sum 文件，其详细罗列了当前项目直接或间接依赖的所有模块版本，并写明了那些模块版本的 SHA-256 哈希值以备 Go 在今后的操作中保证项目所依赖的那些模块版本不会被篡改。



