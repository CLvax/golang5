# gopath和go mod的区别？

> Go 1.11之前golang的包管理模式为gopath，该模式下似乎无法对依赖的版本进行控制，为了解决该问题，就有go mod的方式，在gopath模式下Go 项目必须在GOPATH的src下。不然不能编译，而go mod方式就不用担心这个问题，可以随意创建，不用在src目录下，这是最直观的区别。
