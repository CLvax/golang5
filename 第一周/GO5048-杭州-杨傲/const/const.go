package main

import (
	"fmt"
)

// 定义常量(常量需要初始化值)
const statusNew int = 1
const statusDeleted int = 2

func main() {
	const (
		Monday = 10
		Tuesday
	)
	/*
		若未赋值，则使用最近的一个已使用的常量进行赋值
	*/
	fmt.Println(statusNew, statusDeleted)
	fmt.Println(Monday, Tuesday)

	/*
		枚举值
		iota 在一个小括号内，初始化为0，每调用一次+1
			statusa = iota // 0
			statusb = iota // 1
			statusc = iota // 2
			statusd = iota // 3
	*/
	const (
		statusa = iota
		statusb
		statusc
		statusd
	)
	fmt.Println(statusa, statusb, statusc, statusd)

	const (
		status1 = iota * 100
		status2
		status3
		status4
	)
	fmt.Println(status1, status2, status3, status4)
}
