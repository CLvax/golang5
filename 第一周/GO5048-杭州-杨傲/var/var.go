package main

import "fmt"

// 行注释
/*
   块注释
*/

// 全局变量
// var定义变量
// var 标识符（变量名称） 类型（变量的类型）
// string 表示字符串
// 若未设置则用零值进行初始化
var name string = "atlantis"

/*
  1、var flag type
  2、var flag type = value
  3、var flag = value
*/

func main() {
	// 局部变量
	// 标识符在局部内只能定义一次
	var (
		age    int = 25
		weight     = 145
	)

	var name string = "atlantis2"
	fmt.Println(name, age)
	// 作用域 说明标识符的使用范围 {}
	{
		name = "atlantis3"
	}

	// height := 180 // 短声明 var height = 180
	// 短声明只能用在函数内部
	height, weight := 180, 145

	/*
		        var age, weight, height int
				var age, weight, height int = 1， 2， 3
				var age, weight, height  = 1， ""， 3

	*/

	fmt.Println(name, age, weight, height)                                  // 打印内容后会自动换行
	fmt.Print(name, age, weight, height)                                    // 打印内容后不会加换行符
	fmt.Printf("我叫%s,我的年龄是%d，我的体重是%T，我的身高是%d\n", name, age, weight, height) // 通过占位符进行标量填充
}

// 显示打印标量的数据类型   %T
// 字符串的占位符          %s
// 布尔类型的占位符        %t
// 中文的占位符            %q
// 二进制的占位符          %b
// 十进制数字的占位符      %d
// 十六进制数字的占位符    %x
// 八进制数字的占位符      %o
// byte类型的占位符        %c
// unicode类型的占位符     %U
// 浮点类型的占位符        %f
// 科学计数类型的占位符     %e
// 自动转换f和e的占位符     %g
// ' 是字节、码点
// " 是字符串
