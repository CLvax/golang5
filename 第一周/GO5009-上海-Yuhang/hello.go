/*
Description:  练习hello world
Author:       Yuhang
date:         2021.06.02
*/
package main

import "fmt"

func main(){
    fmt.Println("Hello,world")
}

// 可以尝试下不同的打印方法