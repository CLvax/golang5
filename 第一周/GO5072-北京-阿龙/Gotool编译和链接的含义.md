# Go Tool 中编译、链接的过程含义

新建样例工程

![image-20210601183247687](C:\Users\Along\AppData\Roaming\Typora\typora-user-images\image-20210601183247687.png)

1. 我们先编写pkg包: day1/compile/pkg/demo.go

```
package pkg

import "fmt"

func Demo() {
        fmt.Println("This is demo for go install")
}
```

2. 接下来我们需要编写我们的mian包(程序入口): day1/compile/main.go

```
package main

import "day1/compile/pkg"

func main() {
        pkg.Demo()
}
```

3. 编译并安装我们的pkg包

```
go env -w GO111MODULE=auto // 为了让go install 命令能正常生成静态库到pkg目录下, 我们首先需要关闭go mod
go install day1/compile/pkg  // 使用go install 安装依赖包, 安装过后静态库会放置到 GOPATH下的pkg/<platform>下
```

4. 编译main.go

```
cd /day1/compile  // 切换到compile目录下 进行编译与链接
go tool compile -I /d/Golang/pkg/windows_amd64 main.go // 编译原文件, 指定静态库搜索目录
```

5. 链接main.o

```
go tool link -o main.exe -L /d/Golang/pkg/windows_amd64/  main.o // 将目标文件和静态库链接成一个二进制可执行文件
```

6. 运行我们连接生成的二进制可执行文件

```
./main.exe
```

