# GO版本变化

#### Go1兼容性协议 

官方地址：https://golang.google.cn/doc/go1compat

在Go1版本下，新版本对老版本代码兼容

#### 1.11

**官方地址**：https://golang.google.cn/doc/go1.11

- module机制的实验性引入，以试图解决长久以来困扰Gopher们的包依赖问题；
- 增加对[WebAssembly](https://webassembly.org/)的支持，这样以后Gopher们可以通过Go语言编写前端应用了。

#### 1.12

**官方地址**：https://golang.google.cn/doc/go1.12

- Go Module正式支持
- 更低的 STW 延迟，STW 明显下降，由350微秒降至250微秒左右
- 对 Minimum Mutator Utilization 图的支持
- 初步的 TLS 1.3 支持

#### 1.13

**官方地址**：https://golang.google.cn/doc/go1.13

- 数字字面量
- 越界索引报错完善
- Go Module：golang加入了三个环境变量来共同控制modules的行为，GOPROXY,GOSUMDB,GOPRIVATE
- 错误处理革新
- 反射增加判断字面量是否为0值

#### 1.14

**官方地址**：https://golang.google.cn/doc/go1.14

- defer性能大幅提升
- goroutine支持异步抢占
- time.Timer定时器性能得到大幅提升

#### 1.15

**官方地址**：https://golang.google.cn/doc/go1.15

- 全新的链接器，提升了编译速度
- 编译出的二进制程序减小4%~5%左右 

#### 1.16

**官方地址**：https://golang.google.cn/doc/go1.16

- Go 1.16 主要是 bugfix 和稳定性的提升，并没有重大的特性变化；
- Go 1.16 对多平台多指令架构做了更好的兼容，特别是对 2020 年 Apple 发布的 M1 芯片；
- Cgo tool 不再对 C 的位域做转换，这里要注意；
- Vet 新增了一些告警检测，让我们的语法检查更全面和安全；
- 链接阶段进一步提高了性能，链接速度更快并且耗费资源更少；
- Go 1.16 支持了静态资源文件的内嵌；
- 标准库 io/util 废弃，存量功能迁移到语义更明确的包，比 io 和 os ；
- 标准库 net 对于域名解析的行为和 musl-based 的系统一致；
- Unicode 支持升级为 13.0.0；

