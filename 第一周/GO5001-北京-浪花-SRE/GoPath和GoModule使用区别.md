# GoPath和Go Module使用区别

### GOPATH介绍

**1、GOPATH目录**

~~~powershell
bin //用来存放编译后的可执行文件
pkg //用于存放编译后生成的归档文件
src //用来存放go源码文件
~~~

**2、GOPATH的缺点**

第三方套件只要不是官方程式库，都需要放置在`GOPATH/src`的路径下才可以使用。

go get最常用在当我们想用别人公开在GitHub上的套件，可以帮我们从网路上clone到`GOPATH/src`里面。虽然这样很方便，但是你会发现`GOPATH/src`下的程式码会很复杂，除了有你自己开发的专案资料夹，也包含其他第三方程式库的专案资料夹。

**我们给不同的项目设置不同的GoPath，优点非常明显：**

便于管理项目，每个项目都是不同的GoPath，这对于我们管理多个Golang项目而言，能够非常清晰的处理项目结构。如果我们把所有项目都放在同一个GoPath的src包下，那么项目的结构就会变得非常混乱，难以管理。

**但是当我们需要依赖第三方的包的时候，不同的项目设置不同的GoPath的缺点也非常明显：**

1. 第三方依赖的包和我们自己的Golang包混在一起，会给我们的项目文件管理带来一定的麻烦。
2. 不同的GoPath都需要下载依赖，那么磁盘中重复的依赖就会非常多，会占用我们大量的磁盘空间。

### GO Module介绍

为了解决GOPATH的问题，因此官方在1.11开始推出了Go Modules的功能。Go Modules解决方式很像是Java看到Maven的做法，将第三方程式库储存在本地的空间，并且给程式去引用。

**1、设定GO111MODULE环境变数**

总共可以三种不同的值：

- auto

默认值，go命令会根据当前目录来决定是否启用modules功能。需要满足两种情形：
 该专案目录不在`GOPATH/src/`下
 当前或上一层目录存在go.mod档案

- on

go命令会使用modules，而不会GOPATH目录下查找。

- off

go命令将不会支持module功能，寻找套件如以前GOPATH的做法去寻找。

我是建议要开发Go专案就不再使用GOPATH的功能了，而是采用Go Modules的做法，因此建议都设定为on。
 而采用Go Modules，下载下来的第三方套件都在哪呢？其实就位在`GOPATH/pkg/mod`资料夹里面。

**2、初始化mod**

```
go mod init <module name>
```

`<module name>`可填可不填，不填的话预设就是采用专案资料夹的名称。

在此档案内可以写以下几个关键字：

- module

定义模组路径

- go

定义go语言version

- require

指定依赖的套件，预设是最新版，可以指定版本号

- exclude

排除该套件和其版本

- replace

使用不同的套件版本并替换原有的套件版本注解
 // 单行注解
 /* 多行注解*/
 indirect 代表被间接导入的依赖包

假设现在我要引入GitHub上的`gin-gonic/gin`的套件，如下定义：

```go
module myProject
go 1.16
require github.com/gin-gonic/gin v1.6.3
```

再执行以下指令：

```go
go mod tidy
```

会将需要的套件安装在`GOPATH/pkg/mod`资料夹里面。

~~~go
gin@v1.4.0 gin@v1.6.3 gin@v1.7.1
~~~



除了go.mod之外，go命令还维护一个名为go.sum的文件，其中包含特定模块版本内容的预期加密哈希
 go命令使用go.sum文件确保这些模块的未来下载检索与第一次下载相同的位，以确保项目所依赖的模块不会出现意外更改，无论是出于恶意、意外还是其他原因。 go.mod和go.sum都应检入版本控制。
 go.sum 不需要手工维护，所以可以不用太关注。

只要有开启`go modules`功能，`go get` 就不会像以前一样在`GOPATH/src`下放置套件档案，而是会放在`GOPATH/pkg/mod`里面，并且`go.mod`会写好引入



### 总结

GoPath所引出的问题，就是因为第三方类库的包所导致的，所以我们在有了GoModule之后，GoPath和GoModule就分别负责不同的职责，共同为我们的Golang项目服务。

GoPath我们用来存放我们从网上拉取的第三方依赖包。
GoModule我们用来存放我们自己的Golang项目文件，当我们自己的项目需要依赖第三方的包的时候，我们通过GoModule目录下的一个go.mod文件来引用GoPath目录pkg包下的mod文件夹下的第三方依赖即可。

这样依赖，既解决了原来只能局限在GoPath目录src包下进行编程的问题，也解决了第三方依赖包难以管理和重复依赖占用磁盘空间的问题。


