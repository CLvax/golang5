go1.16

1. GOMODULE默认开启on

   如果要设置成自动

   ```go env -w GO111MODULE=auto```

官方计划1.17后取消支持GOPATH；1.17计划取消GO111MOUDLE 选项，原来使用GOPATH的项目需要尽快迁移。

2. embed包

   导入embed包可以使用//go:embed  指定模式，导入静态文件 作为 string，[]byte, FS 。

```
//go:embed
```

2. 添加 mac arm64 支持
3. bug fix，编译速度提高25%，节省15%内存

go1.15

go1.14

go1.13  

go1.12  2019-02-25



