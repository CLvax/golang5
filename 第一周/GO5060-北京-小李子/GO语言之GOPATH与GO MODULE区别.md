# GOPATH
## GOROOT是什么？
> 在进行go语言安装的时候， 实际上是安装GO编译器和GO标准库， 二者位于同一个安装包中。所以，GOROOT实际上是指Go语言安装目录的环境变量，属于GO语言顶级目录

## GOPATH是什么？
> 在安装Go语言时候，安装程序默认会设置GOPATH环境变量，linux系统是在/root/go
> 与GOROOT不同的是，GOPATH环境变量指向用户域，这样每个用户都拥有自己的工作空间而互不干扰。用户的项目需要位于GOPATH路径下的src目录中。所以，可以理解GOPATH是作为用户工作空间目录来使用的，它属于用户域范畴

## 依赖查找
> 当某个包需要引用其他包的时候，编译器就会依次从GOROOT/src和GOPATH/src中进行查找，如果在GOROOT/src中找到了就不会再去GOPATH/src中进行查找了。所以，GOROOT优先级永远大于GOPATH，即GOROOT属于GO语言顶级目录

## GOPATH的缺点
> GOPATH的优点是简单，在一个项目中，只要设立了GOPATH/src 然后建立项目，项目包中的依赖都是从GOPATH中进行查找建立关系的。但如果在不同的项目中引用了同一个第三方包，但又恰巧使用的是不同版本，那这样GOPATH显然是应对不来的。

- 简单但单一
- 所有项目查找路径都是从GOPATH/src开始
- 多个项目不同环境时配置费劲，只能借助vendor
## 如何手动编译一个程序
```bash
# GOPATH 路径： /home/GoProjects/
# 项目包路径：  day1/compile/pkg
go install day1/compile/pkg  # 此时，在/home/GoProjects/pkg/linux_amd64/day1/compile/pkg.a 生成pkg.a

go tool compile -I /home/GoProjects/pkg/linux_amd64 main.go  # 在执行目录中生成main.o文件

go tool link -o main -L /home/GoProjects/pkg/linux_amd64 main.o # 进行链路二进制文件 生成main 二进制文件

./main # 执行二进制文件

```
# GO MODULE
> 说明： GO MODULE 并不是 基于GOPATH基础上延伸出来的产物，相比下它更像是一种全新的依赖管理

主要解决了两个重要问题

- 准确记录项目依赖
- 可重复的构建

module: 一组package的集合

- 一个仓库包含一个或多个package
- 每个module包含一个或多个package
- 每个package包含一个或多个源文件



## 与GOPATH区别

- 在项目中生成go.mod文件来准确记录项目依赖关系
- 一个module是一组包的集合
- 外部依赖包都存放到GOPATH/src/mod中
- 真正实现项目与项目的外部依赖隔离，精确到小版本
## 如何使用GO MODULE?
```bash
go env GO111MODULE # 查看当前GO MODULE是否开启？
# "" 未开启
# off 未开启
# auto 开启，与GOPATH兼容模式
# ON 开启，如果项目是GOPATH模式的将会报错

# 设置GO MODULE开启
go env -w GO111MODULE=on or auto

# 初始化项目module
go mod init [module]项目名或其他标识
# 比如： go mod init github.com/ligz/gomodule
# 如果init后不加内容，则go mod init会尝试从版本控制系统或import的注释中猜测一个(go版本在1.11时可以，1.13以后将会报错)。推荐指定module

#初始化后会在项目目录中生成一个go.mod文件

# 如果当引入外部依赖时，比如使用go get 下载第三方插件时，会在go.mod统计目录中生成一个go.sum文件，文件中记录依赖包的Hash值

# 其实使用gomod下载外部依赖时，真正的依赖包是存放在GOPATH/pkg/mod 目录中

```


