## 一、GOPATH

### 1.概念

GOPATH 是 go 语言中使用的一个`环境变量`，他使用绝对路径提供`项目的工作目录`。用于存放`源代码`、`依赖包`和`二进制程序`。

查看 gopath `go env GOPATH`

### 2.特点

`go modle` 出现之前源代码只能放在 `$GOPATH/src`目录下

### 3.目录结构

 1.`src` 存放源代码  (比如：.go .c .h .s等） 

 2.`pkg` 编译后生成的文件 ( 比如：.a）

 3.`bin` 编译后生成的可执行文件  

> bin 和 pkg目录可以不创建，go 命令会自动创建, 如：
>
> go install hello.go  会创建  $HOME/go/bin/hello.exe
>
> go get -v golang.org/x/tools/gopls  会创建  $HOME/go/pkg/
>
> 只需要创建src目录即可 



## 二、GOMODULE

### 1.概念

`gomodule`  是Go1.11版本之后官方推出的版本管理工具，并且从Go1.13版本开始，`gomodule` 将是Go语言默认的`依赖管理工具`。

### 2.特点

- `go module` 出现的目的之一就是为了解决 `GOPATH` 的问题，也就相当于是抛弃 `GOPATH` 了。以前项目必须在`$GOPATH/src` 里进行，现在Go 允许在`$GOPATH/src` 外的任何目录下使用 go.mod 创建项目。
- 随着模块一起推出的还有模块代理协议（Module proxy protocol），通过这个协议我们可以实现 Go 模块代理（Go module proxy），也就是依赖镜像。
- Tag 必须遵循语义化版本控制，如果没有将忽略 Tag，然后根据你的 Commit 时间和哈希值再为你生成一个假定的符合语义化版本控制的版本号。
- `go module` 还默认认为，只要你的主版本号不变，那这个模块版本肯定就不包含 Breaking changes。
- Global Caching 这个主要是针对 `go module` 的全局缓存数据说明，如下：
  - 同一个模块版本的数据只缓存一份，所有其他模块共享使用。
  - 目前所有模块版本数据均缓存在 `$GOPATH/pkg/mod`和 `$GOPATH/pkg/sum` 下，未来或将移至 `$GOCACHE/mod`和`$GOCACHE/sum` 下( 可能会在当 `$GOPATH` 被淘汰后)。
  - 可以使用 `go clean -modcache` 清理所有已缓存的模块版本数据。

### 3.配置

#### 3.1 GO111MODULE

配置命令：`go env -w GO111MODULE=auto`

> 要启用 `go module` 支持首先要设置环境变量 `GO111MODULE` ，通过它可以开启或关闭模块支持，它有三个可选值：`off`、`on`、`auto`，默认值是`auto`。
>
> 1. `GO111MODULE=off`禁用模块支持，编译时会从`GOPATH`和`vendor`文件夹中查找包。
> 2. `GO111MODULE=on`启用模块支持，编译时会忽略`GOPATH`和`vendor`文件夹，只根据 `go.mod`下载依赖。
> 3. `GO111MODULE=auto`，当项目在`$GOPATH/src`外且项目根目录有`go.mod`文件时，开启模块支持。
>
> 简单来说，设置`GO111MODULE=on`之后就可以使用`go module`了，以后就没有必要在GOPATH中创建项目了，并且还能够很好的管理项目依赖的第三方包信息。
>
> 使用 go module 管理依赖后会在项目根目录下生成两个文件`go.mod`和`go.sum`。



#### 3.2 GOPROXY

配置命令： `go env -w GOPROXY=https://goproxy.cn,direct`

> 这个环境变量主要是用于设置 Go 模块代理，它的值是一个以英文逗号 “,” 分割的 Go module proxy 列表，默认是proxy.golang.org，国内访问不了。
>
> 其中值列表中的 “direct” 为特殊指示符，用于指示 Go 回源到模块版本的源地址去抓取(比如 GitHub 等)，当值列表中上一个 Go module proxy 返回 404 或 410 错误时，Go 自动尝试列表中的下一个，遇见 “direct” 时回源，遇见 EOF 时终止并抛出类似 “invalid version: unknown revision…” 的错误。

### 4.在项目中使用go module

#### 4.1既有项目

如果需要对一个已经存在的项目启用`go module`，可以按照以下步骤操作：

1. 在项目目录下执行`go mod init`，生成一个`go.mod`文件。
2. 执行`go get`，查找并记录当前项目的依赖，同时生成一个`go.sum`记录每个依赖库的版本和哈希值。

#### 4.2新项目

对于一个新创建的项目，我们可以在项目文件夹下按照以下步骤操作：

1. 执行`go mod init 项目名`命令，在当前项目文件夹下创建一个`go.mod`文件。
2. 手动编辑`go.mod`中的require依赖项或执行`go get`自动发现、维护依赖。

### 5.常用命令

- `go mod init` 初始化模块，例如 `go mod init gitee.com/patience/hello`
- `go build, go test` 和其它构建命令会自动为 go.mod 添加新的依赖
- `go get` 改依赖关系的所需版本(或添加新的依赖关系)
- `go list -m all` 列出当前模块及其所有依赖项
- `go mod tidy` 拉取缺少的模块，移除不用的模块

### 6.不常用命令

- `go mod download` 下载依赖包到本地 cache
- `go mod edit` 编辑 go.mod 文件，选项有 -json、-require 和 -exclude，可以使用帮助 go help mod edit
- `go mod graph` 打印模块依赖图
- `go mod vendor` 将依赖复制到 vendor 目录
- `go mod verify` 验证依赖是否正确
- `go mod why` 解释为什么需要依赖

1. 

## 三、总结

***如果老师看到了的话帮我指正下哈，不确定总结的对不对。谢谢！***

```
gopath 在 gomodle 出现之前，几乎包含了所有的项目管理工作，gopath 目录中存储了包括 源代码、依赖包 和 二
进制程序，默认不支持依赖包的版本管理，以前开发的程序重新构建后可能现在使用的依赖包已经不是之前的依赖包了，使
得项目的维护成本变高，另一方面不支持通过代理的方式下载依赖包对于国内用户非常痛苦。

gomudle 主要为项目的依赖包管理，使得项目的开发可以脱离 gopath 目录之外的位置开发。并支持依赖包的版本管
理，不同的项目可以共享依赖包而不冲突。额外提供的 GOPROXY 功能间接的解决了国内用户不能下载被墙了的依赖包
```
