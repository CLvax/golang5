package main //声明 main包，表明当前是一个可执行程序
import "fmt" //导入内置fmt
func main() {
	fmt.Println("Hello World") //打印 Hello World!
}

// 可以尝试下不同的打印方法