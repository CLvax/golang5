package main

import "fmt"

func helloworld() string {
	return "Hello World!!"
}

func main() {
	fmt.Println(helloworld())
}

// 可以尝试下不同的打印方法