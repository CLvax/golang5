# Go Path和Go Module区别

[go env -w](https://golang.google.cn/cmd/go/#hdr-Print_Go_environment_information)

## GOPATH环境变量
    Go path用于解析导入语句，由go build包实现并记录。

    Go Path中列出每个目录都必须有规定结构：
        src目录包含源代码。 go/src/foo/bar/{x,y}.go
        pkg目录保存已安装的包对象。 go/bin/bar
        bin目录保存已编译的命令。 go/pkg/foo/bar.a
    
    在GO PATH模式中，GOPATH变量是可能包含Go代码的目录列表，在模块感知模式下，模块缓存存储在pkg/mod第一个GOPATH目录的子目录中。如果未设置，默认为Go用户主目录的子目录。

## GO111MODULE环境变量
    可以通过go env -w GO111MODULE = [on | off | auto]
    on：即使没有go.mod文件存在，也在模块感知模式下运行。
    off：忽略go.mod文件存在。
    auto：如果go.mod文件存在于当前目录或任何父目录，则以模块感知模式运行，在go1.15以前默认。

## 个人想法
    由于学习的比较浅显，还未具体测试出两者的区别。