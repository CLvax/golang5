# Golang版本说明

https://golang.google.cn/doc/devel/release

## Go发布历史
go1 - 2012.03.28
    主版本：
    https://golang.google.cn/doc/go1.html

    go1.0.1 - 2012.04.25发布，修复一个可能导致内存损坏的逃逸分析错误。 
        https://golang.org/issue/3545

    go1.0.2 - 2012.06.13发布，修复使用结构或数组键的映射实现中的两个错误： 问题 3695和 问题 3573。它还包括许多次要代码和文档修复。 
        https://golang.org/issue/3695 
        https://golang.org/issue/3573
    
    go1.0.3（2012 年 9 月 21 日发布）包括次要代码和文档修复。
        https://github.com/golang/go/commits/release-branch.go1

go1.1 - 2013.05.13
    主版本：
    https://golang.google.cn/doc/go1.1

    go1.1.1（2013 年 6 月 13 日发布）包括几个编译器和运行时错误修复。
        https://github.com/golang/go/commits/go1.1.1
    
    go1.1.2（2013年8月13日发布）包括修复到gc编译器和cgo，并且bufio，runtime， syscall，和time包。
        https://github.com/golang/go/commits/go1.1.2
        如果您在ARM或386架构的Linux下使用包syscallGetrlimit和Setrlimit函数：
            https://golang.org/issue/5949
            https://golang.org/cl/11803043

go1.2 - 2013.12.01
    https://golang.google.cn/doc/go1.2

    go1.2.1（发布2014年3月2日），包括漏洞修复的runtime，net和database/sql包。
        https://github.com/golang/go/commits/go1.2.1
    
    go1.2.2（2014 年 5 月 5 日发布）包含一个 安全修复程序 ，该修复程序会影响二进制发行版中包含的 Tour 二进制文件（感谢 Guillaume T）。
        https://github.com/golang/go/commits/go1.2.2

go1.3 - 2014.06.18
    https://golang.google.cn/doc/go1.3

    go1.3.1（发布2014年8月13日），包括bug修复编译器和runtime，net和crypto/rsa包。
        https://github.com/golang/go/commits/go1.3.1
    
    go1.3.2（2014 年 9 月 25 日发布）包括对 cgo 和 crypto/tls 包的错误修复。
        https://github.com/golang/go/commits/go1.3.2
    
    go1.3.3（2014 年 9 月 30 日发布）包括对 cgo、运行时包和 nacl 端口的进一步错误修复。
        https://github.com/golang/go/commits/go1.3.3

go1.4 - 2014.12.10
    https://golang.google.cn/doc/go1.4

    go1.4.1（发布2015年1月15日），包括bug修复链接和log，syscall和runtime包。
        https://github.com/golang/go/issues?q=milestone%3AGo1.4.1
    
    go1.4.2（2015年2月17日发布）包括错误修正到go命令时，编译器和链接，以及runtime，syscall，reflect，和math/big包。
        https://github.com/golang/go/issues?q=milestone%3AGo1.4.2
    
    go1.4.3（2015 年 9 月 22 日发布）包括对net/http包的安全修复和对包的错误修复runtime。
        https://github.com/golang/go/issues?q=milestone%3AGo1.4.3

go1.5 - 2015.08.19
    https://golang.google.cn/doc/go1.5

    go1.5.1（发布2015-09-08）包括bug修复编译器，汇编器，和fmt，net/textproto，net/http，和 runtime包。
        https://github.com/golang/go/issues?q=milestone%3AGo1.5.1
    
    go1.5.2（发布2015年12月2日），包括漏洞修复的编译器，链接，和mime/multipart，net和runtime 包。
        https://github.com/golang/go/issues?q=milestone%3AGo1.5.2
    
    go1.5.3（2016 年 1 月 13 日发布）包括对math/big影响crypto/tls包的包的安全修复。
        https://golang.org/s/go153announce
    
    go1.5.4（2016 年 4 月 12 日发布）包括两个安全修复程序。它包含与 Go 1.6.1 相同的修复并同时发布。
        https://github.com/golang/go/issues?q=milestone%3AGo1.6.1

go1.6 - 2016.02.17
    https://golang.google.cn/doc/go1.6

    go1.6.1（2016 年 4 月 12 日发布）包括两个安全修复程序。
        https://github.com/golang/go/issues?q=milestone%3AGo1.6.1
    
    go1.6.2（发布2016年4月20日），包括修复了编译器，运行时工具，文档，以及mime/multipart，net/http和 sort包。
        https://github.com/golang/go/issues?q=milestone%3AGo1.6.2
    
    go1.6.3（2016 年 7 月 17 日发布）包括在 CGI 环境中使用时对net/http/cgi包和net/http包的安全修复 。
        https://github.com/golang/go/issues?q=milestone%3AGo1.6.3
    
    go1.6.4（2016 年 12 月 1 日发布）包括两个安全修复程序。它包含与 Go 1.7.4 相同的修复并同时发布。
        https://github.com/golang/go/issues?q=milestone%3AGo1.7.4

go1.7 - 2016.08.15
    https://golang.google.cn/doc/go1.7

    go1.7.1（2016年9月7日发布）包括修复给编译器，运行时，文档，和compress/flate，hash/crc32， io，net，net/http， path/filepath，reflect，和syscall 包。
        https://github.com/golang/go/issues?q=milestone%3AGo1.7.1
    
    不应使用 go1.7.2。它被标记但未完全发布。由于最后一分钟的错误报告，发布被推迟。请改用 go1.7.3，并参考下面的更改摘要。

    go1.7.3（释放二○一六年十月一十九日）包括修复给编译器，运行时，以及crypto/cipher，crypto/tls， net/http，和strings包。
        https://github.com/golang/go/issues?q=milestone%3AGo1.7.3
    
    go1.7.4（2016 年 12 月 1 日发布）包括两个安全修复程序。
        https://github.com/golang/go/issues?q=milestone%3AGo1.7.4
    
    go1.7.5（2017 年 1 月 26 日发布）包括对编译器、运行时以及crypto/x509和time包的修复。
        https://github.com/golang/go/issues?q=milestone%3AGo1.7.5
    
    go1.7.6（2017 年 5 月 23 日发布）包含与 Go 1.8.2 相同的安全修复程序，并同时发布。
        https://github.com/golang/go/issues?q=milestone%3AGo1.8.2

go1.8 - 2017.02.16
    https://golang.google.cn/doc/go1.8

    go1.8.1（2017年4月7日发布）包括修复给编译器，连接器，运行时，文档，go命令和crypto/tls， encoding/xml，image/png，net， net/http，reflect，text/template，和time包。
        https://github.com/golang/go/issues?q=milestone%3AGo1.8.1
    
    go1.8.2（2017 年 5 月 23 日发布）包含对crypto/elliptic软件包的安全修复 。
        https://github.com/golang/go/issues?q=milestone%3AGo1.8.2
    
    go1.8.3（2017 年 5 月 24 日发布）包括对编译器、运行时、文档和database/sql包的修复。
        https://github.com/golang/go/issues?q=milestone%3AGo1.8.3
    
    go1.8.4（2017 年 10 月 4 日发布）包括两个安全修复程序。它包含与 Go 1.9.1 相同的修复并同时发布。
        https://github.com/golang/go/issues?q=milestone%3AGo1.8.4
    
    go1.8.5（2017 年 10 月 25 日发布）包括对编译器、链接器、运行时、文档、go命令crypto/x509和net/smtp包的修复。它包括对 Go 1.8.4 中引入的错误的修复，该错误go get 在某些条件下破坏了非 Git 存储库。
        https://github.com/golang/go/issues?q=milestone%3AGo1.8.5
    
    go1.8.6（2018 年 1 月 22 日发布）包含与math/big Go 1.9.3相同的修复并同时发布。
        https://github.com/golang/go/issues?q=milestone%3AGo1.8.6
    
    go1.8.7（2018 年 2 月 7 日发布）包含“go get”的安全修复程序。它包含与 Go 1.9.4 相同的修复并同时发布。
        https://github.com/golang/go/issues?q=milestone%3AGo1.8.7

go1.9 - 2017.08.24
    https://golang.google.cn/doc/go1.9

    go1.9.1（2017 年 10 月 4 日发布）包括两个安全修复程序。
        https://github.com/golang/go/issues?q=milestone%3AGo1.9.1+label%3ACherryPickApproved

    go1.9.2（2017年10月25日发布）包括修复给编译器，连接器，运行时，文档，go命令，以及crypto/x509，database/sql，log，和net/smtp包。它包括对 Go 1.9.1 中引入的错误的修复，该错误go get 在某些条件下破坏了非 Git 存储库。
        https://github.com/golang/go/issues?q=milestone%3AGo1.9.2+label%3ACherryPickApproved

    go1.9.3（2018年1月22日发布）包括修复给编译器，运行时，以及database/sql，math/big，net/http，和net/url包。
        https://github.com/golang/go/issues?q=milestone%3AGo1.9.3+label%3ACherryPickApproved

    go1.9.4（2018 年 2 月 7 日发布）包含“go get”的安全修复程序。
        https://github.com/golang/go/issues?q=milestone%3AGo1.9.4+label%3ACherryPickApproved

    go1.9.5（2018 年 3 月 28 日发布）包括对编译器、go 命令和net/http/pprof包的修复。
        https://github.com/golang/go/issues?q=milestone%3AGo1.9.5+label%3ACherryPickApproved

    go1.9.6（2018 年 5 月 1 日发布）包括对编译器和 go 命令的修复。
        https://github.com/golang/go/issues?q=milestone%3AGo1.9.6+label%3ACherryPickApproved

    go1.9.7（2018 年 6 月 5 日发布）包括对 go 命令以及crypto/x509和strings包的修复。特别是，它 为 vgo transition 的 go 命令增加了最少的支持。
        https://go.googlesource.com/go/+/d4e21288e444d3ffd30d1a0737f15ea3fc3b8ad9
        https://github.com/golang/go/issues?q=milestone%3AGo1.9.7+label%3ACherryPickApproved

go1.10 - 2018.02.16
    https://golang.google.cn/doc/go1.10

go1.11 - 2018.08.24
    https://golang.google.cn/doc/go1.11

go1.12 - 2019.02.25
    https://golang.google.cn/doc/go1.12

go1.13 - 2019.09.03
    https://golang.google.cn/doc/go1.13

go1.14 - 2020.02.25
    https://golang.google.cn/doc/go1.14

go1.15 - 2020.08.11
    https://golang.google.cn/doc/go1.15

go1.16 - 2021.02.16
    https://golang.google.cn/doc/go1.16
